package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DeckTest {

    @Test
    public void testCreate() {
        assertEquals(52, Deck.createDeck(1).size());
        assertEquals(104, Deck.createDeck(2).size());
        assertEquals(208, Deck.createDeck(4).size());
        assertEquals(52 * 10, Deck.createDeck(10).size());

        assertEquals(52 * 10, Deck.createDeck(11).size());
        assertEquals(52, Deck.createDeck(-1).size());

        List<Card> deck = Deck.createDeck(2);
        int i = 0;

        for (int deckN = 0; deckN < 2; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    assertEquals(suit, deck.get(i).getSuit());
                    assertEquals(rank, deck.get(i).getRank());
                    assertEquals(rank.getCost(), deck.get(i).getCost());
                    i++;
                }
            }
        }
    }

    @Test
    public void testCostOf() {
        List<Card> deck = Deck.createDeck(1);
        List<Card> hand = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            hand.add(deck.get(0));
            deck.remove(0);
        }

        Assert.assertEquals(16, Deck.costOf(hand));
        Assert.assertEquals(364, Deck.costOf(deck));


        deck = Deck.createDeck(1);
        Assert.assertEquals(380, Deck.costOf(deck));

        deck = Deck.createDeck(2);
        Assert.assertEquals(760, Deck.costOf(deck));

        deck = Deck.createDeck(3);
        Assert.assertEquals(1140, Deck.costOf(deck));
    }

    @Test
    public void testCostOfError() {
        List<Card> hand = new ArrayList<>();
        Assert.assertEquals(0, Deck.costOf(hand));

        hand = null;
        try {
            Assert.assertEquals(null, Deck.costOf(hand));
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }
    }
}
