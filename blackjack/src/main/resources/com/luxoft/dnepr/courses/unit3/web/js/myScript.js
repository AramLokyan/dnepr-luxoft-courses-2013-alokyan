var localhost = "http://localhost:8081/service";
var gameOver = false;
$(document).ready(function () {
    $('#requestMore').click(function () {
        $.ajax({url: localhost, data: {method: 'requestMore'}
        }).done(function (response) {
                var json = $.parseJSON(response);
                if (!gameOver) {
                    showCards(json.myhand, "#myHandHolder");
                    if (!json.result) {
                        $('#msg').text("LOOSE!");
                        gameOver = true;
                    }
                }
            });
    });
    $('#stop').click(function () {
        $.ajax({url: localhost, data: {method: 'stop'}
        }).done(function (response) {
                var json = $.parseJSON(response);
                if (!gameOver) {
                    $('#dealersHandHolder').empty();
                    showCards(json.dealershand, "#dealersHandHolder");
                    $('#msg').text(json.winstate);
                }
                gameOver = true;
            });
    });
    $('#newGame').click(function () {
        $.ajax({url: localhost, data: {method: 'newGame'}
        }).done(function (response) {
                var root = $.parseJSON(response);
                gameOver = false;
                clearTable();
                showCards(root.myhand, "#myHandHolder");
                showCards(root.dealershand, "#dealersHandHolder");
            });
    });
    function clearTable() {
        $('#dealersHandHolder').empty();
        $('#myHandHolder').empty();
        $('#msg').text("");
    }
    function showCards(cards, place) {
        $.each(cards, function () {
            $("<img>").appendTo(place).attr('src', 'img/cards/' + this['suit'].toLowerCase() + '/' + this['rank'] + '.jpg');
        });
    }
});

