package com.luxoft.dnepr.courses.unit3.view;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: (AramLokyan)\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры

     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)

     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)

     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        switch (command) {
            case Command.HELP: {
                printHelp();
                return true;
            }
            case Command.EXIT: {
                return false;
            }
            case Command.MORE: {
                return executeMore(controller);
            }
            case Command.STOP:{
                executeStop(controller);
                return false;
            }
            default:{
                output.println("Invalid command");
                return true;
            }
        }
    }

    private boolean executeMore(GameController controller) {
        boolean more = controller.requestMore();
        printState(controller);

        if (!more) {
            output.println();
            printFinalMessage(controller);
            return false;
        }
        return true;
    }

    private void executeStop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:");
        output.println();
        printState(controller);
        output.println();
        printFinalMessage(controller);
    }



    private void printHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void printFinalMessage(GameController controller) {
        switch (controller.getWinState()) {
            case WIN: {
                output.println("Congrats! You win!");
                break;
            }
            case LOOSE: {
                output.println("Sorry, today is not your day. You loose.");
                break;
            }
            case PUSH: {
                output.println("Push. Everybody has equal amount of points.");
                break;
            }
        }
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();

        format(myHand);

        List<Card> dealersHand = controller.getDealersHand();

        format(dealersHand);
    }

    private void format(List<Card> hand) {
        for (Card card : hand) {
            output.print(card.getRank().getShortName() + " ");
        }

        output.println("(total " + Deck.costOf(hand) + ")");
    }
}
