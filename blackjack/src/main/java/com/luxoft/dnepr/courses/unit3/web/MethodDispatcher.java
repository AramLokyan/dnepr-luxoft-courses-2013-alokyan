package com.luxoft.dnepr.courses.unit3.web;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

import java.util.List;

public class MethodDispatcher {

    /**
     * @param request
     * @param response
     * @return response or <code>null</code> if wasn't able to find a method.
     */
    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        }
        switch (method) {
            case "requestMore": return requestMore(response);
            case "newGame": return newGame(response);
            case "stop": return stop(response);
            default: return null;
        }
    }

    private Response requestMore(Response response) {
        response.write("{\"result\": " + GameController.getInstance().requestMore());
        response.write(", \"myhand\": ");

        List hand = GameController.getInstance().getMyHand();
        writeHand(response, hand.subList(hand.size() - 1, hand.size()));
        response.write(", \"total\": " + Deck.costOf(hand));
        response.write("}");

        return response;
    }

    private Response stop(Response response) {
        GameController.getInstance().requestStop();
        response.write("{\"winstate\": \"" + GameController.getInstance().getWinState());
        response.write("\", \"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");

        return response;
    }


    private Response newGame(Response response) {
        GameController.getInstance().newGame();
        response.write("{\"myhand\": ");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(", \"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");

        return response;
    }


    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("[");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{\"rank\": \"");
            response.write(card.getRank().getShortName());
            response.write("\", \"suit\": \"");
            response.write(card.getSuit().name());
            response.write("\"}");
        }
        response.write("]");
    }

}