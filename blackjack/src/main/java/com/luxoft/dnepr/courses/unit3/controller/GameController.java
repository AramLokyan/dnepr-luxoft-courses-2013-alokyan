package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameController {
    private static GameController controller;

    private List<Card> dealersHand;
    private List<Card> myHand;
    private List<Card> deck;

    private GameController() {
        // инициализация переменных тут
        myHand = new ArrayList<>();
        dealersHand = new ArrayList<>();
        deck = Deck.createDeck(1);
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }
        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        myHand.clear();
        dealersHand.clear();

        shuffler.shuffle(deck);

        takeCardTo(myHand);
        takeCardTo(myHand);

        takeCardTo(dealersHand);
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (Deck.costOf(myHand) > 21 || deck.isEmpty()) {
            return false;
        } else {
            takeCardTo(myHand);
            return Deck.costOf(myHand) <= 21;
        }
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(dealersHand) <= 17) {
            takeCardTo(dealersHand);
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {

        int myCost = Deck.costOf(myHand);
        int dealersCost = Deck.costOf(dealersHand);


        if (myCost > 21) {
            return WinState.LOOSE;

        } else if (myCost < dealersCost && dealersCost <= 21) {
            return WinState.LOOSE;

        } else if (myCost < dealersCost && dealersCost > 21) {
            return WinState.WIN;

        } else if (myCost == dealersCost) {
            return WinState.PUSH;

        } else if (myCost > dealersCost && dealersCost <= 21) {
            return WinState.WIN;

        } else {
            return WinState.LOOSE;
        }
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return myHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealersHand;
    }

    private void takeCardTo(List<Card> hand) {
            hand.add(deck.get(0));
            deck.remove(0);
    }
}
