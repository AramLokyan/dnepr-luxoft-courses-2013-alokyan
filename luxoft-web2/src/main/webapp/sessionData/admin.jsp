<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        #stats {
            margin-left: 30%;
        }
        table{
            border: black solid 1px;

        }
        #loggout {
            float: right;
        }
    </style>
</head>
<body>

<div id="loggout"><a href="/AuthServlet?logged=false">Log Out>></a></div>
<div id="stats">
    <table>
        <th>
            <tr>
                <td>Parametr</td>
                <td>Value</td>
            </tr>

        </th>
        <tr>
            <td>Active Sessions</td>
            <td><%=request.getServletContext().getAttribute("activeSessions")%>
            </td>
        </tr>
        <tr>
            <td>Active Sessions(ROLE user)</td>
            <td><%=request.getServletContext().getAttribute("activeUsers")%>
            </td>
        </tr>
        <tr>
            <td>Active Sessions(ROLE admin)</td>
            <td><%=request.getServletContext().getAttribute("activeAdmins")%>
            </td>
        </tr>
        <tr>
            <td>Total count of HttpRequests</td>
            <td><%=request.getServletContext().getAttribute("totalHttp")%>
            </td>
        </tr>
        <tr>
            <td>Total count of POST HttpRequest</td>
            <td><%=request.getServletContext().getAttribute("totalPost")%>
            </td>
        </tr>
        <tr>
            <td>Total count of GET HttpRequest</td>
            <td><%=request.getServletContext().getAttribute("totalGet")%>
            </td>
        </tr>
        <tr>
            <td>Total count of Other HttpRequest</td>
            <td><%=request.getServletContext().getAttribute("totalOther")%>
            </td>
        </tr>
    </table>
</div>

</body>
</html>