<%@ page import="com.luxoft.dnepr.courses.regular.unit13.entites.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        #hello {
            margin-left: 50%;
        }
        #loggout {
            float: right;
        }
    </style>
</head>
<body>

<div id="loggout"><a href="/AuthServlet?logged=false">Log Out>></a></div>
<span id="hello">Hello <%=request.getSession(false).getAttribute("user")%></span>

</body>
</html>