package com.luxoft.dnepr.courses.regular.unit13.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 16.12.13
 * Time: 1:51
 * To change this template use File | Settings | File Templates.
 */
public class RequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        ServletContext context = servletRequestEvent.getServletContext();
        System.out.println("RL:" + context.getAttribute("totalHttp"));
        ((AtomicInteger)context.getAttribute("totalHttp")).incrementAndGet();
        System.out.println("RL:" + context.getAttribute("totalHttp"));
    }
}
