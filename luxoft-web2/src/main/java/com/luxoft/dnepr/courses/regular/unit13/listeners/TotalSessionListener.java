package com.luxoft.dnepr.courses.regular.unit13.listeners;

import com.luxoft.dnepr.courses.regular.unit13.entites.User;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 16.12.13
 * Time: 2:29
 * To change this template use File | Settings | File Templates.
 */
public class TotalSessionListener implements HttpSessionListener {
    private static AtomicInteger count = new AtomicInteger(0);
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        ServletContext context = session.getServletContext();
        context.setAttribute("activeSessions", count.incrementAndGet());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        ServletContext context = session.getServletContext();
        context.setAttribute("activeSessions", count.decrementAndGet());
    }
}
