package com.luxoft.dnepr.courses.regular.unit13.listeners;

import com.luxoft.dnepr.courses.regular.unit13.entites.User;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 16.12.13
 * Time: 1:55
 * To change this template use File | Settings | File Templates.
 */
public class SessionListener implements HttpSessionAttributeListener {
    private static AtomicInteger userCount = new AtomicInteger(0);
    private static AtomicInteger adminCount = new AtomicInteger(0);
    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session = httpSessionBindingEvent.getSession();
        ServletContext context = session.getServletContext();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            if (user.getRole().equals("admin")) {
                context.setAttribute("activeAdmins", adminCount.incrementAndGet());
            } else {
                context.setAttribute("activeUsers", userCount.incrementAndGet());
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session = httpSessionBindingEvent.getSession();
        ServletContext context = session.getServletContext();
        User user = (User) httpSessionBindingEvent.getValue();
        if (user != null) {
            if (user.getRole().equals("admin")) {
                ((AtomicInteger)context.getAttribute("activeAdmins")).decrementAndGet();
            } else {
                ((AtomicInteger)context.getAttribute("activeUsers")).decrementAndGet();
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
    }
}
