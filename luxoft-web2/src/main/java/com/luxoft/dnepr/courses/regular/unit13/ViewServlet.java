package com.luxoft.dnepr.courses.regular.unit13;

import com.luxoft.dnepr.courses.regular.unit13.entites.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ViewPost");
        User user = (User) req.getSession().getAttribute("user");
        if (user != null){
            if (user.getRole().equals("admin")){
                req.getRequestDispatcher("/sessionData/admin.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("/WEB-INF/user.jsp").forward(req, resp);
            }
        }
    }
}
