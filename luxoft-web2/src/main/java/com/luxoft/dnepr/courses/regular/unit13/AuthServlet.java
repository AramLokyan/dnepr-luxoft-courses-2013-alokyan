package com.luxoft.dnepr.courses.regular.unit13;

import com.luxoft.dnepr.courses.regular.unit13.entites.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AuthServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");

        User user = new User(login, password, role);
        System.out.println("Auth" + user);
        Set<User> users = (HashSet<User>) getServletContext().getAttribute("users");
        System.out.println(users);
        if (login != null && !login.isEmpty() &&
                password != null && !password.isEmpty() &&
                users.contains(user)) {
            for(User u : users){
                if (u.equals(user)) user = u;
            }
            session.setAttribute("logged", true);
            session.setAttribute("user", user);
            req.getRequestDispatcher("/ViewServlet").forward(req, resp);
        } else {
            if ("false".equals(req.getParameter("logged"))) {
                session.invalidate();
            }
            if ((login == null && password == null) || ("".equals(login) && "".equals(password))) {
                req.setAttribute("result", null);
            } else {
                req.setAttribute("result", "Login or password is incorrect");
            }
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }



}
