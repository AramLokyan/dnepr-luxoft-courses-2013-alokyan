package com.luxoft.dnepr.courses.regular.unit13.listeners;

import com.luxoft.dnepr.courses.regular.unit13.entites.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Listener implements ServletContextListener {
    private ServletContext context = null;
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        context = servletContextEvent.getServletContext();
        Set<User> users = new HashSet<>();
        InputStream is = context.getResourceAsStream("META-INF/users.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = null;
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        Element root = doc.getDocumentElement();
        NodeList children = root.getChildNodes();
        root.getElementsByTagName("user");
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                users.add(new User(((Element) child).getAttribute("name"),
                        ((Element) child).getAttribute("password"),
                        ((Element) child).getAttribute("role")));
            }
        }
        context.setAttribute("users", users);
        AtomicInteger activeUsers = new AtomicInteger(0);
        AtomicInteger activeAdmins = new AtomicInteger(0);
        AtomicInteger totalHttp = new AtomicInteger(0);
        context.setAttribute("activeUsers", activeUsers);
        context.setAttribute("activeAdmins", activeAdmins);
        context.setAttribute("totalHttp", totalHttp);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        context.removeAttribute("users");
    }
}
