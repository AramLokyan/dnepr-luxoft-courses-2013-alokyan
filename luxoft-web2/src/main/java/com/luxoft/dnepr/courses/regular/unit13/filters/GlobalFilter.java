package com.luxoft.dnepr.courses.regular.unit13.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 16.12.13
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
public class GlobalFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession(true);
        Boolean loggedAttribute = (Boolean) session.getAttribute("logged");
        System.out.println("GlobalFilter: loggedAttribute = " + loggedAttribute);

        if (loggedAttribute != null && loggedAttribute) {
            System.out.println("GlobalFilter: logged IN" + session.getAttribute("user"));
            chain.doFilter(request, response);
        } else {
            System.out.println("GlobalFilter: Logged OUT" );
            req.getRequestDispatcher("/AuthServlet").forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
