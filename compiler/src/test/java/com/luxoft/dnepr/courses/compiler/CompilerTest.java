package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;
import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
        assertCompiled(3.925559, "  9.123    /2.324 ");
        assertCompiled(4.4575, "2.223+2.2345");
        assertCompiled(0, "0.0+0.0");
        assertCompiled(0, "0/1");
    }

    @Test
    public void testSimpleError() {
        assertCompiled(Double.NaN, "0/0");
        assertCompiled(Double.POSITIVE_INFINITY, "1/0");

        try {
            assertCompiled(0, " 1.111 12 + 3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " A.023 + 3.14 ");
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " 0.023 % 3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " 0.023 ++ 3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " 0.023 + +3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " +0.023 ++ +3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "+0.023");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "0 + 1.");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, ".0.023");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " 0.+023 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, null);
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
        assertCompiled(4, " 2*2/2*2/2*2/2*2");
        assertCompiled(1, " (2+2)/(2+2)");
        assertCompiled(1, "  100 / 2 / 5 / 10");
        assertCompiled(12, "(((2 + 2 )) * 3)");
        assertCompiled(14, "2    +  2 * (2 + 2 * 2)");
        assertCompiled(6, "  2 + 2 * 2 ");
        assertCompiled(1, "1");
        assertCompiled(1.25, "(1.25)");
        assertCompiled(1.25, "0 + (1.25)");
    }

    @Test
    public void testComplexError() {
        try {
            assertCompiled(0, "2  2  2  *  +");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "2 + (1 /2) (3-5)");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "(");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, ")");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "()");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, "2 + ()");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
        try {
            assertCompiled(0, " 0.023.12 + 3.14 ");
            Assert.fail();
        } catch (CompilationException e) {
            System.out.println(e.getMessage());
        }
    }
}
