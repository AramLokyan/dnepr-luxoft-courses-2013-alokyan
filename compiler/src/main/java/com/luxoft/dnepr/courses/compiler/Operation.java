package com.luxoft.dnepr.courses.compiler;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 23.10.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */

public enum Operation {
    OPEN('(', 1),
    CLOSE(')', 1),
    PLUS('+', 2),
    MINUS('-', 2),
    MULTIPLY('*', 3),
    DIVIDE('/', 3);

    private int priority;
    private char operation;

    private Operation(char operation, int priority) {
        this.operation = operation;
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public char getOperation() {
        return operation;
    }

    public static int getPriority(char operation) {
        for (Operation o : Operation.values()) {
            if (o.getOperation() == operation) {
                return o.getPriority();
            }
        }
        return -1;
    }

    public static boolean isOperation(char operation) {
        for (Operation o : Operation.values()) {
            if (o.getOperation() == operation) {
                if (o.getPriority() > 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isOperation(String operation) {
        if (operation.length() == 1) {
            for (Operation o : Operation.values()) {
                if (o.getOperation() == operation.charAt(0)) {
                    if (o.getPriority() > 1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isBracket(char operation) {
        for (Operation o : Operation.values()) {
            if (o.getOperation() == operation) {
                if (o.getPriority() == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}

