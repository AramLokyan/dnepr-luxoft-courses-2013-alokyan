package com.luxoft.dnepr.courses.compiler;

import java.util.Stack;
import java.util.regex.Pattern;

public class Formula {
    private String formula;
    private StringBuilder number;
    private Stack<String> formulaElements;
    private static final String DOUBLE_INT = "(\\d+.\\d+)||(\\d+)";

    public Formula(String formula) {
        if (formula == null || formula.isEmpty()) {
            throw new CompilationException("Input formula is null or empty.");
        }
        this.formula = formula.replaceAll("\\s+", " ").trim();
        formulaElements = new Stack<>();
        number = new StringBuilder();
    }

    public boolean isCorrect() {
        int bracketsCount = 0;
        for (int i = 0; i < formula.length(); i++) {
            char symbol = formula.charAt(i);
            if (Character.isDigit(symbol) || symbol == '.') {
                i = readNumberSequence(i);
                checkNumberPosition(i);

            } else if (Operation.isOperation(symbol)) {
                checkOperationPosition(i);

            } else switch (symbol) {
                case '(': {
                    bracketsCount++;
                    checkOpenPosition();
                    break;
                }
                case ')': {
                    bracketsCount--;
                    checkClosePosition();
                    break;
                }
                case ' ': {
                    checkSpacePosition(i);
                    break;
                }
                default: {
                    throw new CompilationException("Syntax error. Unknown symbol \"" + symbol + "\".");
                }
            }
        }

        checkBracketsCount(bracketsCount);
        return true;
    }

    public Stack<String> getFormulaElements() {
        isCorrect();
        return formulaElements;
    }

    public Stack<String> getReversePolishNotation() {
        Stack<String> formulaElements = getFormulaElements();
        Stack<String> reverseNotation = new Stack();
        Stack<Character> stack = new Stack();

        for (String element : formulaElements) {
            char firstSymbol = element.charAt(0);
            if (Character.isDigit(firstSymbol)) {
                reverseNotation.push(element);

            } else if (Operation.isOperation(firstSymbol)) {
                if (stack.isEmpty()) {
                    stack.push(firstSymbol);

                } else if (Operation.getPriority(stack.peek()) < Operation.getPriority(firstSymbol)) {
                    stack.push(firstSymbol);
                } else {
                    while (!stack.isEmpty() &&
                            Operation.getPriority(stack.peek()) >= Operation.getPriority(firstSymbol)) {
                        reverseNotation.push(Character.toString(stack.pop()));
                    }
                    stack.push(firstSymbol);
                }
            } else switch (element) {
                case "(": {
                    stack.push('(');
                    break;
                }
                case ")": {
                    while (stack.peek() != '(') {
                        reverseNotation.push(Character.toString(stack.pop()));
                    }
                    stack.pop();
                    break;
                }
                default: {
                    throw new CompilationException("Syntax error. Failed to create reverse polish notation. Unknown symbol \"" + firstSymbol + "\"");
                }
            }
        }

        while (!stack.isEmpty()) {
            reverseNotation.push(Character.toString(stack.pop()));
        }
        return reverseNotation;
    }

    private void checkClosePosition() {
        if (!formulaElements.isEmpty() && !Character.isDigit(formulaElements.peek().charAt(0)) && formulaElements.peek().charAt(0) != ')') {
            throw new CompilationException("Syntax error. Close bracket check failed. Digit expected, but \"" + formulaElements.peek().charAt(0) + "\" found.");
        }
        formulaElements.push(")");
    }

    private void checkOpenPosition() {
        if (!formulaElements.isEmpty() && !Operation.isOperation(formulaElements.peek().charAt(0)) && formulaElements.peek().charAt(0) != '(') {
            throw new CompilationException("Syntax error. Open bracket check failed. Operation expected, but \"" + formulaElements.peek().charAt(0) + "\" found.");
        }
        formulaElements.push("(");
    }

    private void checkBracketsCount(int bracketsCount) {
        if (bracketsCount > 0) {
            throw new CompilationException("Syntax error. Closing bracket missing.");
        } else if (bracketsCount < 0) {
            throw new CompilationException("Syntax error. Opening bracket missing.");
        }
    }

    private void checkOperationPosition(int i) {
        if (formulaElements.isEmpty() || i == formula.length() - 1) {
            throw new CompilationException("Syntax error. Operation check failed. Operation is not allowed at the start or end of statement.");
        }
        char leftElement = formulaElements.peek().charAt(0);

        if (Character.isDigit(leftElement) || leftElement == ')') {
            char rightElement = formula.charAt(i + 1) == ' ' ? formula.charAt(i + 2) : formula.charAt(i + 1);

            if (rightElement == '(' || Character.isDigit(rightElement)) {
                formulaElements.push(Character.toString(formula.charAt(i)));
            } else {
                throw new CompilationException("Syntax error. Operation check failed. Digit expected, but \"" + rightElement + "\" found.");
            }
        } else {
            throw new CompilationException("Syntax error. Operation check failed. Operation is not allowed after \"" + leftElement + "\".");
        }
    }

    private void checkSpacePosition(int i) {
        char leftElement = formulaElements.peek().charAt(0);
        char rightElement = formula.charAt(i + 1);
        if (Character.isDigit(leftElement) && Character.isDigit(rightElement)) {
            throw new CompilationException("Syntax error. Space check failed. Operation is not found between \"" + leftElement + "\" and \"" + rightElement + "\".");
        }
        if (leftElement == '(' && Operation.isOperation(rightElement)) {
            throw new CompilationException("Syntax error. Space check failed. Number is not found between \"" + leftElement + "\" and \"" + rightElement + "\".");
        }
        if (Operation.isOperation(leftElement) && rightElement == ')') {
            throw new CompilationException("Syntax error. Space separator check failed. Number is not found between \"" + leftElement + "\" and \"" + rightElement + "\".");
        }
    }

    private void checkNumberPosition(int i) {
        if (formulaElements.isEmpty()) {
            formulaElements.push(number.toString());
        } else {
            char leftElement = formulaElements.peek().charAt(0);
            if (leftElement == '(' || Operation.isOperation(leftElement)) {

                if (i == formula.length() - 1) {
                    formulaElements.push(number.toString());
                } else {
                    char rightElement = formula.charAt(i + 1) == ' ' ? formula.charAt(i + 2) : formula.charAt(i + 1);

                    if (rightElement == ')' || Operation.isOperation(rightElement)) {
                        formulaElements.push(number.toString());
                    } else {
                        throw new CompilationException("Syntax error. Number check failed. Operation expected, but \"" + formula.charAt(i + 1) + "\" found.");
                    }
                }
            } else {
                throw new CompilationException("Syntax error. Number check failed. Operation expected, but \"" + formula.charAt(i + 1) + "\" found.");
            }
        }
    }

    private int readNumberSequence(int i) {
        number = new StringBuilder();
        while (i < formula.length() &&
                (Character.isDigit(formula.charAt(i)) || formula.charAt(i) == '.')) {
            number.append(formula.charAt(i++));
        }
        checkNumberValidation(number.toString());
        return --i;
    }

    private void checkNumberValidation(String s) {
        if (!Pattern.compile(DOUBLE_INT).matcher(s).matches()) {
            throw new CompilationException("Syntax error. \"" + number + "\" is not a valid number.");
        }
    }
}
