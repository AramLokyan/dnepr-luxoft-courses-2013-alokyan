package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        Formula formula = new Formula(input);
        Stack<String> stack = formula.getReversePolishNotation();

        for (String element : stack){
            try{
                addCommand(result, VirtualMachine.PUSH, Double.parseDouble(element));

            } catch (NumberFormatException e){
                if (element.length() != 1){
                    throw new CompilationException("Compile error." , new IllegalArgumentException("\""+element + "\" is not a valid operation."));
                }

                switch(element.charAt(0)){
                    case '+':{
                        addCommand(result, VirtualMachine.ADD);
                        break;
                    }
                    case '-':{
                        addCommand(result, VirtualMachine.SWAP);
                        addCommand(result, VirtualMachine.SUB);
                        break;
                    }
                    case '*': {
                        addCommand(result, VirtualMachine.MUL);
                        break;
                    }
                    case '/': {
                        addCommand(result, VirtualMachine.SWAP);
                        addCommand(result, VirtualMachine.DIV);
                        break;
                    }
                    default: {
                        throw new CompilationException("Compile error. Valid operation not found.");
                    }
                }
            }
        }
        addCommand(result, VirtualMachine.PRINT);

        return result.toByteArray();
    }



    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
