package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AuthServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        Map<String, String> users = (HashMap<String, String>) getServletContext().getAttribute("users");
        if (login != null && !login.isEmpty() &&
                password != null && !password.isEmpty() &&
                users.containsKey(login) &&
                users.get(login).equals(password)) {

            session.setAttribute("logged", true);
            session.setAttribute("user", login);
            req.getRequestDispatcher("/ViewServlet").forward(req, resp);
        } else {
            if ("false".equals(req.getParameter("logged"))) {
                session.invalidate();
            }
            if ((login == null && password == null) || ("".equals(login) && "".equals(password))) {
                req.setAttribute("result", null);
            } else {
                req.setAttribute("result", "Login or password is incorrect");
            }
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }



}
