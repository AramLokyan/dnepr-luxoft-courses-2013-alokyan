package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

public class DummyServlet extends HttpServlet {
    private static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json; charset=utf-8");

        if (name == null || age == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            String s = map.putIfAbsent(name, age);
            if (s != null) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + name + " already exists\"}");
            } else {
                resp.setStatus(201);
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        if (name == null || age == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            if (!map.containsKey(name)) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + name + " does not exist\"}");
            } else {
                map.put(name, age);
                resp.setStatus(202);
            }
        }
    }
}
