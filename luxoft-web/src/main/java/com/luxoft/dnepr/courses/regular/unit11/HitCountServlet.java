package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

public class HitCountServlet extends HttpServlet{
    private static AtomicInteger hitCount = new AtomicInteger(0);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String json = "{\"hitCount\": " + hitCount.incrementAndGet() + "}";
        writer.print(json);
    }
}
