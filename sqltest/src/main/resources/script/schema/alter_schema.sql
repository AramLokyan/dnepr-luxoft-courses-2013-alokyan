CREATE TABLE IF NOT EXISTS makers (
  maker_id INT AUTO_INCREMENT,
  maker_name VARCHAR(50) NOT NULL,
  maker_adress VARCHAR(200),
  CONSTRAINT pk_printer PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (1, 'A', 'AdressA');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (2, 'B', 'AdressB');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (3, 'C', 'AdressC');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (4, 'D', 'AdressD');
INSERT INTO makers(maker_id, maker_name, maker_adress) VALUES (5, 'E', 'AdressE');

ALTER TABLE product ADD COLUMN maker_id INT NOT NULL AFTER type;

UPDATE product, makers
SET product.maker_id = makers.maker_id
WHERE product.maker = makers.maker_name;

ALTER TABLE product DROP COLUMN maker;

ALTER TABLE product
ADD CONSTRAINT fk_product_makers
FOREIGN KEY (maker_id)
REFERENCES makers (maker_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS printer_type (
  type_id INT,
  type_name VARCHAR(50) NOT NULL,
  CONSTRAINT pk_printer PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name) VALUES (1, 'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES (2, 'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES (3, 'Matrix');

ALTER TABLE printer ADD COLUMN type_id INT NOT NULL AFTER price;

UPDATE printer, printer_type
SET printer.type_id=printer_type.type_id
WHERE printer.type = printer_type.type_name;

ALTER TABLE printer DROP COLUMN type;

ALTER TABLE printer
ADD CONSTRAINT fk_printer_type
FOREIGN KEY (type_id)
REFERENCES printer_type (type_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE printer CHANGE COLUMN color color CHAR(1) NOT NULL DEFAULT 'y' ;

ALTER TABLE laptop ADD INDEX ind_laptop_price (price ASC);
ALTER TABLE printer ADD INDEX ind_printer_price (price ASC);
ALTER TABLE pc ADD INDEX ind_pc_price (price ASC);



