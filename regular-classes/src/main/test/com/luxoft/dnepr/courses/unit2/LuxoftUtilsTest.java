package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 11.10.13
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */
public class LuxoftUtilsTest {
    @Test
    public void testSortArray() {
        String[] lettersUnsorted = {"g", "z", " ", "f", "H", "o", "Z", "A"};
        String[] lettersTrue = {" ", "A", "H", "Z", "f", "g", "o", "z"};
        String[] lettersFalse = {"z", "o", "g", "f", "Z", "H", "A", " "};
        Assert.assertArrayEquals(lettersTrue, LuxoftUtils.sortArray(lettersUnsorted, true));
        Assert.assertArrayEquals(lettersFalse, LuxoftUtils.sortArray(lettersUnsorted, false));

        String[] unsorted = {"abC", "aBc", "Abc", "ABc", "AbC", "aBC", "abc", "ABC", "aBc", " ", " A", " a"};
        String[] sortedTrue = {" ", " A", " a", "ABC", "ABc", "AbC", "Abc", "aBC", "aBc", "aBc", "abC", "abc"};
        String[] sortedFalse = {"abc", "abC", "aBc", "aBc", "aBC", "Abc", "AbC", "ABc", "ABC", " a", " A", " "};
        Assert.assertArrayEquals(sortedTrue, LuxoftUtils.sortArray(unsorted, true));
        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(unsorted, false));
    }

    @Test
    public void testSortArrayError() {
        try {
            Assert.assertArrayEquals(null, LuxoftUtils.sortArray(null, true));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        String[] unsorted = new String[0];
        try {
            Assert.assertArrayEquals(null, LuxoftUtils.sortArray(unsorted, true));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        String[] empty = {"", "", "", ""};
        Assert.assertArrayEquals(empty, LuxoftUtils.sortArray(empty, true));

        String[] spaceUnsorted = {"", " ", "", "", ""};
        String[] spaceSorted = {" ", "", "", "", ""};
        Assert.assertArrayEquals(spaceSorted, LuxoftUtils.sortArray(spaceUnsorted, false));

        try {
            String[] digitsUnsortedError = {"4", "1", "21", "5", "2", "245", "13"};
            Assert.assertArrayEquals(null, LuxoftUtils.sortArray(digitsUnsortedError, true));
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }
    }

    @Test
    public void testWordAverageLength() {
        String words = " one two three four five sixteen comparable  ";
        Assert.assertEquals(5.142857, LuxoftUtils.wordAverageLength(words), 0.000001);

        words = "I have a dog";
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength(words), 0.000001);
    }

    @Test
    public void testWordAverageLengthError() {
        String empty = "";
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(empty), 0.000001);

        String nullString = null;
        try{
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(nullString), 0.000001);
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        String space = " ";
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(space), 0.000001);

        String wordsSpaces = "  This   string      has   lots    of      spaces  ";
        Assert.assertEquals(4.166666, LuxoftUtils.wordAverageLength(wordsSpaces), 0.000001);
    }

    @Test
    public void testReverseWords() {
        String wordsStraight = "abc dog space";
        String wordsReversed = "cba god ecaps";
        Assert.assertEquals(wordsReversed, LuxoftUtils.reverseWords(wordsStraight));

        wordsStraight = "  abc    dog  space    ";
        wordsReversed = "  cba    god  ecaps    ";
        Assert.assertEquals(wordsReversed, LuxoftUtils.reverseWords(wordsStraight));
    }

    @Test
    public void testReverseWordsError() {
        String empty = "";
        Assert.assertEquals("", LuxoftUtils.reverseWords(empty));

        String nullString = null;
        try{
        Assert.assertEquals("", LuxoftUtils.reverseWords(nullString));
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        String digits = "1 2 3 4 5";
        try{
            Assert.assertEquals("", LuxoftUtils.reverseWords(digits));
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        String space = " ";
        Assert.assertEquals(" ", LuxoftUtils.reverseWords(space));
    }

    @Test
    public void testGetCharEntries() {
        Assert.assertArrayEquals(new char[]{'I', 'a', 'd', 'e', 'g', 'h', 'o', 'v'}, LuxoftUtils.getCharEntries("I have a dog"));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
        Assert.assertArrayEquals(new char[]{'Z', 'L', 'A', 'F', 'z', 'k', 'a', 'p', 'c'}, LuxoftUtils.getCharEntries(" aa c  LLL  kkk pp zzzz F AA ZZZZ "));
    }

    @Test
    public void testGetCharEntriesError() {
        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries("abc#"));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries("ФЫВапрол"));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries("45678"));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(""));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(null));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        try {
            Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(new String()));

        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }

        Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries("      "));
    }

    @Test
    public void testCalculateOverallArea() {
        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Circle(2.0));
        figures.add(new Square(2.0));
        figures.add(new Hexagon(2.0));
        Assert.assertEquals(26.958675, LuxoftUtils.calculateOverallArea(figures), 0.000001);

        figures = new ArrayList<Figure>();
        figures.add(new Square(2.0));
        Assert.assertEquals(4.0, LuxoftUtils.calculateOverallArea(figures), 0.000001);
    }

    @Test
    public void testCalculateOverallAreaError() {
        List<Figure> figures = new ArrayList<Figure>();
        Assert.assertEquals(0, LuxoftUtils.calculateOverallArea(figures), 0.000001);

        figures = null;
        try {
            Assert.assertEquals(0, LuxoftUtils.calculateOverallArea(figures), 0.000001);
        } catch (IllegalArgumentException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StackTraceElement element = stackTrace[1];
            System.out.println(element.getMethodName() + " line:" + element.getLineNumber() + " " + e.getMessage());
        }
    }
}
