package com.luxoft.dnepr.courses.unit1;

import com.luxoft.dnepr.courses.unit1.LuxoftUtils;
import org.junit.Assert;
import org.junit.Test;


public class LuxoftUtilsTest {
    @Test
    public void testGetMonthName() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
    }

    @Test
    public void testGetMonthNameError() {

        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));

        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName('%', "ru "));  //"ru" with space
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName('#', "en "));  //"en" with space

        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(14, "en"));
        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName('z', "en"));

        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(-5, "ru"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName('a', "ru"));

        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(Integer.MAX_VALUE, "ru"));
        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(Integer.MIN_VALUE, "en"));

        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName('a', null));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName('a', "null"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName('a', ""));
    }

    @Test
    public void testBinaryToDecimal() {
        Assert.assertEquals("0", LuxoftUtils.binaryToDecimal("00000000000 "));
        Assert.assertEquals("1", LuxoftUtils.binaryToDecimal("000001"));
        Assert.assertEquals("5", LuxoftUtils.binaryToDecimal("101"));
        Assert.assertEquals("182", LuxoftUtils.binaryToDecimal("10110110"));

    }

    @Test
    public void testBinaryToDecimalError() {
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("null"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("1010205"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("12345"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("AAF"));
    }

    @Test
    public void testDecimalToBinary() {
        Assert.assertEquals("0", LuxoftUtils.decimalToBinary("0"));
        Assert.assertEquals("1", LuxoftUtils.decimalToBinary("1"));
        Assert.assertEquals("101", LuxoftUtils.decimalToBinary("5"));
        Assert.assertEquals("10110110", LuxoftUtils.decimalToBinary("182"));
        Assert.assertEquals("10110110", LuxoftUtils.decimalToBinary("182 "));

        Assert.assertEquals("1010", LuxoftUtils.decimalToBinary("010"));
    }

    @Test
    public void testDecimalToBinaryError() {
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("-1"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("A"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("12F"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("$ % ^ "));

        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("null"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));
    }

    @Test
    public void testSortArray() {
        int[] unsorted = {9, 5, 8, 1, 4, 7, 3, 6, 2, 0, -1};

        int[] sortedTrue = {-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] sortedFalse = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1};

        Assert.assertArrayEquals(sortedTrue, LuxoftUtils.sortArray(unsorted, true));
        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(unsorted, false));

        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(sortedTrue, false));
        Assert.assertArrayEquals(sortedTrue, LuxoftUtils.sortArray(sortedFalse, true));
    }

    @Test
    public void testSortArrayError() {
        int[] unsorted = new int[0];
        int[] sortedTrue = {};
        int[] sortedFalse = {};
        Assert.assertArrayEquals(sortedTrue, LuxoftUtils.sortArray(unsorted, true));
        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(unsorted, false));

        unsorted = new int[]{1};
        sortedTrue = new int[]{1};
        sortedFalse = new int[]{1};
        Assert.assertArrayEquals(sortedTrue, LuxoftUtils.sortArray(unsorted, true));
        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(unsorted, false));

//        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(null, false));
//        Assert.assertArrayEquals(sortedFalse, LuxoftUtils.sortArray(null, true));
    }
}
