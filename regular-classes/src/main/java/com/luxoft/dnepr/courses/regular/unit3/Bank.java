package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 05.11.13
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) {
        String actualJavaVersion = System.getProperty("java.version");
        if (expectedJavaVersion == null || expectedJavaVersion.isEmpty() || !actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Java version mismatch.");
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        if (!checkTransactionConditions(fromUserId, toUserId, amount)) {
            throw new TransactionException("Transaction conditions are not valid. Check input params.");
        } else {
            UserInterface fromUser = users.get(fromUserId);
            UserInterface toUser = users.get(toUserId);
            WalletInterface toUserWallet = toUser.getWallet();
            WalletInterface fromUserWallet = fromUser.getWallet();

            checkWalletStatus(fromUser, toUser);

            if (fromUserWallet.getAmount().compareTo(amount) < 0){
                throw new TransactionException("User \'" + fromUser.getName() + "\' has insufficient funds (" + fromUserWallet.getAmount().setScale(2, RoundingMode.HALF_UP) + " < " + amount.setScale(2, RoundingMode.HALF_UP) + ")");
            } else if (toUserWallet.getAmount().add(amount).compareTo(toUserWallet.getMaxAmount()) > 0){
                throw new TransactionException("User \'" + toUser.getName() + "\' wallet limit exceeded (" + toUserWallet.getAmount().setScale(2, RoundingMode.HALF_UP) + " + " + amount.setScale(2, RoundingMode.HALF_UP) + " > " + toUserWallet.getMaxAmount().setScale(2, RoundingMode.HALF_UP) + ")");
            }
            fromUserWallet.withdraw(amount);
            toUserWallet.transfer(amount);
        }
    }

    private void checkWalletStatus(UserInterface fromUser, UserInterface toUser) throws TransactionException {
        if (fromUser.getWallet().getStatus() == WalletStatus.BLOCKED){
            throw new TransactionException("User \'" + fromUser.getName() + "\' wallet is blocked");
        } else if (toUser.getWallet().getStatus() == WalletStatus.BLOCKED) {
            throw new TransactionException("User \'" + toUser.getName() + "\' wallet is blocked");
        }
    }

    private boolean checkTransactionConditions(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException {
        if (users != null && !users.isEmpty()){
            if (!users.containsKey(fromUserId)) {
                throw new NoUserFoundException(fromUserId, "No such user found, id = " + fromUserId);
            } else if (!users.containsKey(toUserId)) {
                throw new NoUserFoundException(toUserId, "No such user found, id = " + toUserId);
            } else if (amount != null && amount.intValue() > 0 && fromUserId != toUserId) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
