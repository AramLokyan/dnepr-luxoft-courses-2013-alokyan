package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private List<CompositeProduct> compositeProducts = new ArrayList<>();
    private List<Product> simplePoducts = new ArrayList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        boolean productExists = false;
        for (CompositeProduct compositeProduct : compositeProducts) {
            if (product.equals(compositeProduct.getChild(0))) {
                compositeProduct.add(product);
                productExists = true;
            }
        }
        if (!productExists) {
            CompositeProduct currentProduct = new CompositeProduct();
            currentProduct.add(product);
            compositeProducts.add(currentProduct);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double summ = 0;
        for (CompositeProduct compositeProduct : compositeProducts) {
            summ += compositeProduct.getPrice();
        }
        return summ;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        simplePoducts = new ArrayList<Product>(compositeProducts);

        Collections.sort(simplePoducts, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return (p1.getPrice() - p2.getPrice()) >= 0 ? -1 : 1;
            }
        });
        return simplePoducts;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
