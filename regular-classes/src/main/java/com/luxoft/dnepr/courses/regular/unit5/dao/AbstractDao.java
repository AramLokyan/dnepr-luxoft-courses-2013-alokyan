package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {
    private Map<Long, Entity> storage = EntityStorage.getEntities();
    private Class<E> type;

    public AbstractDao(Class<E> type) {
        this.type = type;
    }

    @Override
    public E save(E entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entity is null");

        } else if (entity.getId() == null) {
            entity.setId(storage.isEmpty() ? 1 : Collections.max(storage.keySet()) + 1);

        } else if (storage.containsKey(entity.getId())) {
            throw new UserAlreadyExist("User with id = " + entity.getId() + " already exists.");
        }
        storage.put(entity.getId(), entity);
        return entity ;
    }

    @Override
    public E update(E entity) {
        if (entity.getId() == null || !storage.containsKey(entity.getId())) {
            throw new UserNotFound("User with id = " + entity.getId() + " is not found.");
        }

        E e = get(entity.getId());
        if (e != null) {
            if (e.getClass() == type){
                storage.put(entity.getId(), entity);
                return entity;
            }
        }
        return null;
    }

    @Override
    public E get(long id) {
        if (storage.get(id) != null) return storage.get(id).getClass() == type ? (E) storage.get(id) : null;
        return null;
    }

    @Override
    public boolean delete(long id) {
        if (get(id) != null) return get(id).getClass() == type ? storage.remove(id) != null : false;
        return false;
    }

}
