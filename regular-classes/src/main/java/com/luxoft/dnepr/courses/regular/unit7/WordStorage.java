package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents word statistics storage.
 */
public class WordStorage {
    private Map<String, Integer> storage = new HashMap<>();

    /**
     * Saves given word and increments count of occurrences.
     *
     * @param word
     */
    public synchronized void save(String word) {
        if (storage.get(word) == null) {
            storage.put(word, 1);
        } else {
            Integer count = storage.get(word);
            storage.put(word, ++count);
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
