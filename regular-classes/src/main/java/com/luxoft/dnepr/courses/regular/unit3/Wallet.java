package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 05.11.13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (amountToWithdraw == null || amountToWithdraw.doubleValue() < 0){
            throw new IllegalArgumentException("Wallet.checkWithdrawal() validation failed. amountToWithdraw is null or incorrect");
        } else if (amount == null || status == null){
            throw new IllegalStateException("Wallet.checkWithdrawal() validation failed. Wallet state is incorrect, some fields are null.");
        }
        if (status == WalletStatus.BLOCKED){
            throw new WalletIsBlockedException(id, "Wallet.checkWithdrawal() validation failed. Wallet is blocked.");
        } else if (amount.compareTo(amountToWithdraw) < 0){
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "Wallet.checkWithdrawal() validation failed. Not enough funds to perform this operation (" + amount.setScale(2, RoundingMode.HALF_UP) + " < " + amountToWithdraw.setScale(2, RoundingMode.HALF_UP) + ")");
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (amountToTransfer == null || amountToTransfer.doubleValue() < 0){
            throw new IllegalArgumentException("Wallet.checkTransfer() validation failed. amountToTransfer is null or incorrect");
        } else if (amount == null || maxAmount==null || status == null){
            throw new IllegalStateException("Wallet.checkTransfer() validation failed. Wallet state is incorrect, some fields are null.");
        }
        if (status == WalletStatus.BLOCKED){
            throw new WalletIsBlockedException(id, "Wallet.checkTransfer() validation failed. Wallet is blocked.");
        } else if (amount.add(amountToTransfer).compareTo(maxAmount) > 0){
            throw new LimitExceededException(id, amountToTransfer, amount, "Wallet.checkTransfer() validation failed. Max wallet funds exceeded.(" + amount.add(amountToTransfer).setScale(2, RoundingMode.HALF_UP) + " > " + maxAmount.setScale(2, RoundingMode.HALF_UP) + ")");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "id=" + id +
                ", amount=" + amount +
                ", status=" + status +
                ", maxAmount=" + maxAmount +
                '}';
    }
}
