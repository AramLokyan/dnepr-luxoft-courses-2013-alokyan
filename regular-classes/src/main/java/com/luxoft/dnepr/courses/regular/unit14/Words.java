package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class Words {
    private Set<String> known = new HashSet<>();
    private Set<String> unknown = new HashSet<>();

    public int getResult(int totalAmount) {
        return totalAmount * (known.size() + 1) / (known.size() + unknown.size() + 1);
    }

    public void addKnown(String word) {
        known.add(word);
    }

    public void addUnknown(String word) {
        unknown.add(word);
    }

}
