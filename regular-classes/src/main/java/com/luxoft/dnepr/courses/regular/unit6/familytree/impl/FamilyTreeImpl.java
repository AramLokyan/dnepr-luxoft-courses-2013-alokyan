package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.StringTokenizer;

public class FamilyTreeImpl implements FamilyTree {

    private static final long serialVersionUID = 3057396458981676327L;
    private Person root;
    private transient long creationTime;

    private FamilyTreeImpl(Person root, long creationTime) {
        this.root = root;
        this.creationTime = creationTime;
    }

    public static FamilyTree create(Person root) {
        return new FamilyTreeImpl(root, System.currentTimeMillis());
    }

    @Override
    public Person getRoot() {
        return root;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        if (root instanceof PersonImpl) {
            out.writeUTF("{\"root\":");
            out.writeUTF(root.toString());
            out.writeUTF("}");
        } else {
            out.defaultWriteObject();
        }

    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.readUTF();
        root = PersonImpl.readPerson(new StringTokenizer(in.readUTF()));
        in.readUTF();
    }

}
