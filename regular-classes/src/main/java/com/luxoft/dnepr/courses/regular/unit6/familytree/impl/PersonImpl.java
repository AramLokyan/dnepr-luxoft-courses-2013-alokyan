package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.*;
import java.util.StringTokenizer;

public class PersonImpl implements Person {
    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public PersonImpl() {
    }

    public PersonImpl(String name, String ethnicity, Person father, Person mother, Gender gender, int age) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.father = father;
        this.mother = mother;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        StringBuilder json = new StringBuilder();
        json.append("{");
        if (name != null) json.append("\"name\":\"").append(name).append("\"");
        if (ethnicity != null)
            json.append(name != null ? "," : "").append("\"ethnicity\":\"").append(ethnicity).append("\"");
        if (father != null)
            json.append(name != null || ethnicity != null ? "," : "").append("\"father\":").append(father);
        if (mother != null)
            json.append(name != null || ethnicity != null || father != null ? "," : "").append("\"mother\":").append(mother);
        if (gender != null)
            json.append(name != null || ethnicity != null || father != null || mother != null ? "," : "").append("\"gender\":\"").append(gender).append("\"");
        if (age != 0)
            json.append(name != null || ethnicity != null || father != null || mother != null || gender != null ? "," : "").append("\"age\":\"").append(age).append("\"");

        return json.append("}").toString();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeUTF(this.toString());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        StringTokenizer tokenizer = new StringTokenizer(in.readUTF());
        PersonImpl temp = readPerson(tokenizer);
        this.name = temp.name;
        this.ethnicity = temp.ethnicity;
        this.father = temp.father;
        this.mother = temp.mother;
        this.gender = temp.gender;
        this.age = temp.age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonImpl person = (PersonImpl) o;

        if (age != person.age) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (father != null ? !father.equals(person.father) : person.father != null) return false;
        if (gender != person.gender) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    public static PersonImpl readPerson(StringTokenizer tokenizer) {
        PersonImpl root = new PersonImpl();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken(":\",");
            switch (token) {
                case "name": {
                    root.setName(tokenizer.nextToken());
                    break;
                }
                case "ethnicity": {
                    root.setEthnicity(tokenizer.nextToken());
                    break;
                }
                case "father": {
                    root.setFather(readPerson(tokenizer));
                    break;
                }
                case "mother": {
                    root.setMother(readPerson(tokenizer));
                    break;
                }
                case "gender": {
                    root.setGender(tokenizer.nextToken().equals("MALE") ? Gender.MALE : Gender.FEMALE);
                    break;
                }
                case "age": {
                    root.setAge(Integer.parseInt(tokenizer.nextToken()));
                    break;
                }
                default: {
                    if (token.endsWith("}")) {
                        return root;
                    }
                }
            }
        }
        return root;
    }

}
