package com.luxoft.dnepr.courses.regular.unit5.model;

public class Redis extends Entity {
    private int weight;

    public Redis() {

    }

    public Redis(int weight) {
        this.weight = weight;
    }

    public Redis(Long id, int weight) {
        super(id);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Redis(" + super.toString() +
                " weight=" + weight +
                ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Redis redis = (Redis) o;

        if (weight != redis.weight) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + weight;
        return result;
    }
}
