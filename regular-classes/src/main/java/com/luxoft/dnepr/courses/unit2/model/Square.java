package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 13.10.13
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
public class Square extends Figure{
    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return Math.pow(side, 2);
    }
}
