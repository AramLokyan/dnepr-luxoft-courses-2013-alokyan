package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class EqualSet<E> implements java.util.Set<E> {
    private Collection<E> data = new LinkedList<E>();

    public EqualSet() {
    }

    public EqualSet(Collection<E> base) {
        addAll(base);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return data.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return data.iterator();
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return data.toArray(a);
    }

    @Override
    public boolean add(E e) {
        if (data.contains(e)) {
            return false;
        } else {
            data.add(e);
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        return data.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return data.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean added = false;
        for (E e : c) {
            if (add(e)) {
                added = true;
            }
        }
        return added;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        return data.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return data.removeAll(c);
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EqualSet equalSet = (EqualSet) o;

        return size() == equalSet.size() ? containsAll(equalSet) : false;
    }

    @Override
    public int hashCode() {
        int hash = 31;
        for (E e : data) {
            if (e != null)
                hash += e.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        return "EqualSet{" +
                "data=" + data +
                '}';
    }
}
