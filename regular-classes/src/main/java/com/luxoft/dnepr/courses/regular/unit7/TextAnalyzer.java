package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class TextAnalyzer implements Runnable {

    private WordStorage wordStorage;
    private BlockingQueue<File> filesQueue;
    private boolean done = false;

    public TextAnalyzer(BlockingQueue<File> filesQueue, WordStorage wordStorage) {
        this.filesQueue = filesQueue;
        this.wordStorage = wordStorage;
    }

    @Override
    public void run() {
        try {
            while (!done) {
                File file = filesQueue.take();
                if (file == FolderAnalyzer.DUMMY) {
                    filesQueue.put(file);
                    done = true;
                } else {
                    analyze(file);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private File analyze(File file) {
        if (file == null) return new File("");
        Scanner scanner = null;
        try {
            scanner = new Scanner(file, "UTF-8");
            scanner.useDelimiter("[^A-Za-zА-Яа-я0-9ё]+");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner.hasNext()) {
            String word = scanner.next();
            wordStorage.save(word);
        }
        scanner.close();
        return file;
    }
}
