package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 13.10.13
 * Time: 20:54
 * To change this template use File | Settings | File Templates.
 */
public abstract class Figure {
    public abstract double calculateArea();
}
