package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 11.10.13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    public static String[] sortArray(String[] array, boolean asc) {

        if (array == null || array.length < 1) {
            throw new IllegalArgumentException("Input array is empty or null, initialize it.");
        }

        for (String s : array){
            validationCheck(s);
        }

        String[] sorted = mergeSort(array);

        if (!asc) {
            for (int i = 0; i < sorted.length / 2; i++) {
                String tmp = sorted[i];
                sorted[i] = sorted[sorted.length - (i + 1)];
                sorted[sorted.length - (i + 1)] = tmp;
            }
        }
        return sorted;
    }

    private static String[] mergeSort(String[] array) {
        String[] sorted;

        if (array.length == 1) {
            sorted = array;
        } else {
            String[] left = Arrays.copyOfRange(array, 0, array.length / 2);
            String[] right = Arrays.copyOfRange(array, left.length, array.length);

            left = mergeSort(left);
            right = mergeSort(right);
            sorted = mergeArrays(left, right);
        }
        return sorted;
    }

    private static void validationCheck(String str) {
        for (char c : str.toCharArray()){
            if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ')){
                throw new IllegalArgumentException("String must consist of [A-Z], [a-z] and space chars. Illegal chars found in <" + str +">");
            }
        }
    }

    private static String[] mergeArrays(String[] left, String[] right) {
        String[] merged = new String[left.length + right.length];

        int leftIndex = 0;
        int rightIndex = 0;
        int mergeIndex = 0;

        while ((leftIndex < left.length) || (rightIndex < right.length)) {

            if (leftIndex == left.length) {
                merged[mergeIndex++] = right[rightIndex++];

            } else if (rightIndex == right.length) {
                merged[mergeIndex++] = left[leftIndex++];

            } else if ((left[leftIndex].compareTo(right[rightIndex])) > 0) {
                merged[mergeIndex++] = right[rightIndex++];

            } else if ((left[leftIndex].compareTo(right[rightIndex])) < 0) {
                merged[mergeIndex++] = left[leftIndex++];

            } else if ((left[leftIndex].compareTo(right[rightIndex])) == 0) {
                merged[mergeIndex++] = right[rightIndex++];
            }
        }
        return merged;
    }

    public static double wordAverageLength(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Input string is null.");
        }

        validationCheck(str);

        String[] words = str.trim().split("\\s+");

        double result = 0;
        for (String word : words) {
            result += word.length();
        }

        return result / words.length;
    }

    public static String reverseWords(String str) {
        if (str == null ) {
            throw new IllegalArgumentException("Input string is null.");
        }

        validationCheck(str);

        StringBuilder result = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                result.append(tmp.reverse());
                tmp.delete(0, tmp.length());
                result.append(str.charAt(i));
            } else {
                tmp.append(str.charAt(i));
                if (i == str.length() - 1) {
                    result.append(tmp.reverse());
                }

            }
        }
        return result.toString();
    }

    public static char[] getCharEntries(String str) {

        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Input string is null or empty.");
        }

        Map<Character, Integer> charEntries = new TreeMap<Character, Integer>();

        for (int i = 0; i < str.length(); i++) {
            char letter = str.charAt(i);

            if ((letter >= 'A' && letter <= 'Z') || (letter >= 'a' && letter <= 'z')){
                if (charEntries.containsKey(letter)) {
                    charEntries.put(letter, charEntries.get(letter) + 1);
                } else {
                    charEntries.put(letter, 1);
                }
            } else {
                if (!Character.isSpaceChar(letter)){
                    throw new IllegalArgumentException("String must consist of [A-Z], [a-z] and space chars. First illegal char found <" + letter + ">");
                }
            }
        }

        Set<Map.Entry<Character, Integer>> sortedChars = new TreeSet<Map.Entry<Character, Integer>>(new CharEntriesComparator());
        sortedChars.addAll(charEntries.entrySet());

        char[] resultChars = new char[sortedChars.size()];
        int i = 0;
        Iterator<Map.Entry<Character, Integer>> iterator = sortedChars.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Character, Integer> next = iterator.next();
            resultChars[i++] = next.getKey();
        }

        return resultChars;
    }

    private static class CharEntriesComparator implements Comparator<Map.Entry<Character, Integer>> {
        @Override
        public int compare(Map.Entry<Character, Integer> c1, Map.Entry<Character, Integer> c2) {

            if ((Character.isLowerCase(c1.getKey()) && Character.isLowerCase(c2.getKey()) ||
                    (Character.isUpperCase(c1.getKey()) && Character.isUpperCase(c2.getKey())))) {
                if (c1.getKey() > c2.getKey()) {
                    if (c1.getValue() > c2.getValue()) {
                        return -1;
                    } else {
                        return 1;
                    }

                } else if (c1.getValue() > c2.getValue()) {
                        return 1;
                    } else {
                        return -1;
                    }

            } else if (Character.isLowerCase(c1.getKey()) && Character.isUpperCase(c2.getKey())) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public static double calculateOverallArea(List<Figure> figures){
        double squareSum = 0;

        if (figures == null){
            throw new IllegalArgumentException("Input list of figures is null. Initialize it.");
        }

        for (Figure figure : figures){
            squareSum += figure.calculateArea();
        }

        return squareSum;
    }
}
