package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 13.10.13
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */
public class Hexagon extends Figure{
    private double side;

    public Hexagon(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return (3 * Math.sqrt(3) * Math.pow(side, 2))/2;
    }
}
