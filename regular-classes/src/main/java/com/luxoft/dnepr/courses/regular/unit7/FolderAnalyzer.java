package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.concurrent.*;

public class FolderAnalyzer implements Callable<List<File>> {

    private File root;
    private BlockingQueue<File> fileQueue;
    private CopyOnWriteArrayList<File> indexedFiles = new CopyOnWriteArrayList<>();
    public static final File DUMMY = new File("");

    public FolderAnalyzer(File root, BlockingQueue<File> fileQueue) {
        this.root = root;
        this.fileQueue = fileQueue;
    }

    @Override
    public List<File> call() {
        try {
            crawl(root);
            fileQueue.put(DUMMY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return indexedFiles;
    }

    private void crawl(File root) throws InterruptedException {
        File[] entries = root.listFiles(new FileFolderFilter());
        if (entries != null) {
            for (File entry : entries) {
                if (entry.isDirectory()){
                    crawl(entry);
                } else {
                    if (!alreadyIndexed(entry)){
                        fileQueue.put(entry);
                        indexedFiles.add(entry);
                    }
                }
            }
        }
    }

    private boolean alreadyIndexed(File entry) {
        return indexedFiles.contains(entry);
    }

    class FileFolderFilter implements FilenameFilter {
        @Override
        public boolean accept(File dir, String name) {
            return new File(dir, name).isDirectory() || name.endsWith(".txt");
        }
    }
}





