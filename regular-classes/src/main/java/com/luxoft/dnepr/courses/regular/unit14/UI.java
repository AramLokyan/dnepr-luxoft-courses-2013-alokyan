package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;


public class UI {
    private Vocabulary vocabulary = new Vocabulary();
    private Words words = new Words();
    private String command;
    private String word;

    public void run() {
        System.out.println("Linguistic analyzer v 1.0, author Tushar Brahmacobalol.\n");
        Scanner scanner = new Scanner(System.in);
        do {
            word = vocabulary.randomWord();
            System.out.println("Do you know translation of this word? \"" + word + "\"");
            command = scanner.next();
            menuChoice();
        } while (!"exit".equals(command));
        System.out.println("Your estimated vocabulary is " + words.getResult(vocabulary.size()) + " words");
    }

    private void menuChoice() {
        switch (command.toLowerCase()) {
            case "help": {
                printHelp();
                break;
            }
            case "y": {
                words.addKnown(word);
                break;
            }
            case "n": {
                words.addUnknown(word);
                break;
            }
            case "exit": {
                break;
            }
            default: {
                System.out.println("Invalid command " + command);
                printHelp();
                break;
            }
        }
    }

    private void printHelp() {
        System.out.println("HELP displays this help menu; \n" +
                "Y/N answers a question (YES or NO); \n" +
                "EXIT quits this program.\n");
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
