package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    public static String getMonthName(int monthOrder, String language) {
        if ("ru".equals(language)) {
            switch (monthOrder) {
                case 1: {
                    return "Январь";
                }
                case 2: {
                    return "Февраль";
                }
                case 3: {
                    return "Март";
                }
                case 4: {
                    return "Апрель";
                }
                case 5: {
                    return "Май";
                }
                case 6: {
                    return "Июнь";
                }
                case 7: {
                    return "Июль";
                }
                case 8: {
                    return "Август";
                }
                case 9: {
                    return "Сентябрь";
                }
                case 10: {
                    return "Октябрь";
                }
                case 11: {
                    return "Ноябрь";
                }
                case 12: {
                    return "Декабрь";
                }
            }
            return "Неизвестный месяц";
        } else if ("en".equals(language)) {
            switch (monthOrder) {
                case 1: {
                    return "January";
                }
                case 2: {
                    return "February";
                }
                case 3: {
                    return "March";
                }
                case 4: {
                    return "April";
                }
                case 5: {
                    return "May";
                }
                case 6: {
                    return "June";
                }
                case 7: {
                    return "July";
                }
                case 8: {
                    return "August";
                }
                case 9: {
                    return "September";
                }
                case 10: {
                    return "October";
                }
                case 11: {
                    return "November";
                }
                case 12: {
                    return "December";
                }
            }
            return "Unknown Month";
        } else {
            return "Unknown Language";
        }
    }

    public static String binaryToDecimal(String binaryNumber) {
        if (null == binaryNumber || binaryNumber.isEmpty()) return "Not binary";
        binaryNumber = binaryNumber.trim();

        int result = 0;
        int power = 0;

        for (int i = binaryNumber.length() - 1; i >= 0; i--) {

            char digitChar = binaryNumber.charAt(i);
            int digit = Character.getNumericValue(digitChar);

            if ((digit == 0) || (digit == 1)) {
                result += digit * Math.pow(2, power);
                power++;
            } else return "Not binary";
        }
        return String.valueOf(result);
    }

    public static String decimalToBinary(String decimalNumber) {
/*
        v1.0
        Negative numbers and octal radix are not supported at this method version
 */
        if (null == decimalNumber || decimalNumber.isEmpty()) return "Not decimal";
        decimalNumber = decimalNumber.trim();

        int quotient = 0;
        int modulo = 0;
        int decimalNumberInt = 0;
        StringBuilder result = new StringBuilder();

        try {
            decimalNumberInt = Integer.parseInt(decimalNumber, 10);
        } catch (NumberFormatException e) {
            return "Not decimal";
        }

        if (decimalNumberInt < 0) return "Not decimal";

        do {
            quotient = decimalNumberInt / 2;
            modulo = decimalNumberInt % 2;
            decimalNumberInt = quotient;
            result.append(modulo);
        } while (quotient != 0);

        return result.reverse().toString();
    }

    public static int[] sortArray(int[] array, boolean asc) {

//        v1.0
//        NullPointerException or IllegalArgumentException are not supported at this method version
        int[] result = array.clone();
        boolean arrayChanged = true;

        while (arrayChanged) {
            arrayChanged = false;
            for (int i = 0; i < result.length - 1; i++) {

                if (asc) {
                    if (result[i] > result[i + 1]) {
                        int temp = result[i];
                        result[i] = result[i + 1];
                        result[i + 1] = temp;
                        arrayChanged = true;
                    }
                } else {
                    if (result[i] < result[i + 1]) {
                        int temp = result[i];
                        result[i] = result[i + 1];
                        result[i + 1] = temp;
                        arrayChanged = true;
                    }
                }
            }
        }

        return result;
    }
}
