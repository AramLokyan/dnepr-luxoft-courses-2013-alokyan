package com.luxoft.dnepr.courses.regular.unit14;

import java.io.*;
import java.net.URL;
import java.util.*;

public class Vocabulary {
    private static final String DEFAULT_FILE_NAME = "sonnets.txt";
    private List<String> vocabulary = new ArrayList<>();

    public Vocabulary() {
        File source = new File(getClass().getResource(DEFAULT_FILE_NAME).getFile());
        analyze(source);
    }

    public Vocabulary(String fileName) {
        File source = new File(getClass().getResource(fileName).getFile());
        analyze(source);
    }

    public int size() {
        return vocabulary.size();
    }

    private void analyze(File file) {
        if (file == null) return;
        Scanner scanner = null;
        try {
            scanner = new Scanner(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        scanner.useDelimiter("[^A-Za-zА-Яа-я0-9ё']+");
        while (scanner.hasNext()) {
            String word = scanner.next();
            if (!vocabulary.contains(word)) vocabulary.add(word);
        }
        scanner.close();
    }

    public String randomWord() {
        Random random = new Random();
        return vocabulary.get(random.nextInt(size()));
    }
}
