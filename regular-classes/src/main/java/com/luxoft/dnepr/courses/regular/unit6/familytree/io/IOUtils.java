package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) throws IOException, ClassNotFoundException {
        return load(new FileInputStream(filename));
    }

    public static FamilyTree load(InputStream is) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(is);
        FamilyTree familyTree = (FamilyTree) in.readObject();
        in.close();
        return familyTree;
    }

    public static void save(String filename, FamilyTree familyTree) {
        try {
            save(new FileOutputStream(filename), familyTree);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) {
        try (ObjectOutputStream output = new ObjectOutputStream(os)) {
            output.writeObject(familyTree);
            output.flush();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
