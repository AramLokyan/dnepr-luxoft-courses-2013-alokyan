package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private WordStorage wordStorage = new WordStorage();

    private BlockingQueue<File> filesQueue = new LinkedBlockingQueue<>();
    private File root;
    private int maxNumberOfThreads;


    public FileCrawler(String rootFolder, int maxNumberOfThreads) {

        this.maxNumberOfThreads = maxNumberOfThreads > 1 ? maxNumberOfThreads : 2;
        root = new File(rootFolder);
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        FutureTask<List<File>> foundFiles = new FutureTask<>(new FolderAnalyzer(root, filesQueue));
        new Thread(foundFiles).start();

        List<Thread> textAnalyzers = new LinkedList<>();
        for (int i = 0; i < maxNumberOfThreads - 1; i++) {
            textAnalyzers.add(new Thread(new TextAnalyzer(filesQueue, wordStorage)));
            textAnalyzers.get(i).start();
        }

        joinAll(textAnalyzers);
        List<File> analyzedFiles = getFoundFiles(foundFiles);
        return new FileCrawlerResults(analyzedFiles, wordStorage.getWordStatistics());
    }

    private List<File> getFoundFiles(FutureTask<List<File>> foundedFiles) {
        List<File> analyzedFiles = null;
        try {
            analyzedFiles = foundedFiles.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return analyzedFiles;
    }

    private void joinAll(List<Thread> textAnalyzers) {
        for (Thread t : textAnalyzers) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}



