package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        return childProducts.isEmpty() ? null : childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        return childProducts.isEmpty() ? null : childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double price = 0;
        double discount = 1;

        if (getAmount() == 2) {
            discount -= 0.05;
        } else if (childProducts.size() >= 3) {
            discount -= 0.10;
        }

        for (Product product : childProducts) {
            price += product.getPrice();
        }

        return price * discount;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public Product getChild(int i) {
        return childProducts.isEmpty() ? null : childProducts.get(i);
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
