package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 03.11.13
 * Time: 0:14
 * To change this template use File | Settings | File Templates.
 */
public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();
    @Test
    public void testClone() throws CloneNotSupportedException {
        Beverage drink = productFactory.createBeverage("bev", "Cola", 10, true);
        Beverage cloned = (Beverage)drink.clone();
        assertTrue(drink.equals(cloned));
        assertEquals(cloned.getCode(), drink.getCode());
        assertEquals(cloned.getName(), drink.getName());
        assertEquals(cloned.getPrice(), drink.getPrice(), 0);
        assertEquals(cloned.isNonAlcoholic(), drink.isNonAlcoholic());
        if (cloned.hashCode() != drink.hashCode()) {
            fail("Beverage.clone() Hash code have to be the same.");
        }
        if (cloned == drink) {
            fail("Beverage.clone() The clone has to be another instance.");
        }
    }

    @Test
    public void testEquals() throws CloneNotSupportedException {
        Beverage drink = productFactory.createBeverage("bev", "Cola", 10, true);
        Beverage cloned = productFactory.createBeverage("bev", "Cola", 10, true);
        assertTrue(drink.equals(cloned));
        assertEquals(cloned.getCode(), drink.getCode());
        assertEquals(cloned.getName(), drink.getName());
        assertEquals(cloned.getPrice(), drink.getPrice(), 0);
        assertEquals(cloned.isNonAlcoholic(), drink.isNonAlcoholic());
    }

    @Test
    public void testEqualsError() throws CloneNotSupportedException {
        Beverage drink = new Beverage();
        Beverage cloned = new Beverage();
        assertTrue(drink.equals(cloned));
        assertEquals(cloned.getCode(), drink.getCode());
        assertEquals(cloned.getName(), drink.getName());
        assertEquals(cloned.getPrice(), drink.getPrice(), 0);
        assertEquals(cloned.isNonAlcoholic(), drink.isNonAlcoholic());
    }

    @Test
    public void testCloneError() throws CloneNotSupportedException {
        Beverage drink = new Beverage();
        Beverage cloned = (Beverage) drink.clone();
        if (cloned.hashCode() != drink.hashCode()) {
            fail("Book.clone() Hash code have to be the same.");
        }
        if (cloned == drink) {
            fail("Book.clone() The clone has to be another instance.");
        }
        assertTrue(drink.equals(cloned));
        assertEquals(cloned.getCode(), drink.getCode());
        assertEquals(cloned.getName(), drink.getName());
        assertEquals(cloned.getPrice(), drink.getPrice(), 0);
        assertEquals(cloned.isNonAlcoholic(), drink.isNonAlcoholic());
    }

    @Test
    public void testGetPrice() {
        Beverage drink = new Beverage("cola", "Coca-Cola", 10, true);
        assertEquals(10, drink.getPrice(), 0);
    }

    @Test
    public void testGetPriceError() {
        Beverage drink = new Beverage("cola", "Coca-Cola", -10, true);
        assertEquals(10, drink.getPrice(), 0);
    }
}
