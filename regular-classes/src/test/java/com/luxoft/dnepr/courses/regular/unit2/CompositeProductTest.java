package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testAddError() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("", "", 0, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage(null, null, 0, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10) * 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);
    }

    @Test
    public void testGetPriceError() {
        CompositeProduct compositeProduct = new CompositeProduct();
        assertEquals(0, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("", "", 0, true));
        assertEquals(0, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage(null, null, 0, true));
        assertEquals(0, compositeProduct.getPrice(), 0);
    }

    @Test
    public void testGetCode() {
        CompositeProduct compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals("cola", compositeProduct.getCode());
    }

    @Test
    public void testGetCodeError() {
        CompositeProduct compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage("", "", 0, true));
        assertEquals("", compositeProduct.getCode());

        compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage(null, null, 0, true));
        assertEquals(null, compositeProduct.getCode());
    }

    @Test
    public void testGetName() {
        CompositeProduct compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals("Coca-cola", compositeProduct.getName());
    }

    @Test
    public void testGetNameError() {
        CompositeProduct compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage("", "", 0, true));
        assertEquals("", compositeProduct.getName());

        compositeProduct = new CompositeProduct();
        compositeProduct.add(factory.createBeverage(null, null, 0, true));
        assertEquals(null, compositeProduct.getName());
    }
}
