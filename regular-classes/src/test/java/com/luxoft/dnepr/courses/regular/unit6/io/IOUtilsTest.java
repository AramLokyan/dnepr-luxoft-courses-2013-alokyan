package com.luxoft.dnepr.courses.regular.unit6.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import junit.framework.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class IOUtilsTest {
    private PersonImpl motherGrandFather = new PersonImpl("Robbert", "american", null, null, Gender.MALE, 72);
    private PersonImpl motherGrandMother = new PersonImpl("Charlotte", "france", null, null, Gender.FEMALE, 65);
    private PersonImpl fatherGrandFather = new PersonImpl("Charlie", "american", null, null, Gender.MALE, 67);
    private PersonImpl fatherGrandMother = new PersonImpl("Gretta", "irish", null, null, Gender.FEMALE, 63);
    private PersonImpl father = new PersonImpl("Jack", "canadian", fatherGrandFather, fatherGrandMother, Gender.MALE, 54);
    private PersonImpl mother = new PersonImpl("Marine", "armenian", motherGrandFather, motherGrandMother, Gender.FEMALE, 51);
    private PersonImpl person = new PersonImpl("Sam", "german", father, mother, Gender.MALE, 25);
    private String fileName = "familyTree.txt";

    @Test
    public void testSave(){
        FamilyTree familyTree = FamilyTreeImpl.create(person);
        IOUtils.save(fileName, familyTree);
    }

    @Test
    public void testSaveSimple(){
        person.setFather(null);
        person.setMother(null);
        person.setName(null);
        person.setEthnicity(null);
        FamilyTree familyTree = FamilyTreeImpl.create(person);
        IOUtils.save(fileName, familyTree);
    }



    @Test
    public void testLoad() throws IOException, ClassNotFoundException {
        testSave();
        FamilyTree familyTree = IOUtils.load(fileName);
        Person root = familyTree.getRoot();
        Assert.assertTrue(root.equals(person));
        Assert.assertEquals(root, person);
    }

    @Test
    public void testLoadError() throws IOException, ClassNotFoundException {
        person = new PersonImpl();
        testSave();
        FamilyTree familyTree = IOUtils.load(fileName);
        Person root = familyTree.getRoot();
        Assert.assertTrue(root.equals(person));
        Assert.assertEquals(root, person);
    }

    @Test
    public void testLoadError2() throws IOException, ClassNotFoundException {
        person = new PersonImpl();
        person.setFather(new PersonImpl());
        person.setMother(new PersonImpl());
        testSave();
        FamilyTree familyTree = IOUtils.load(fileName);
        Person root = familyTree.getRoot();
        Assert.assertTrue(root.equals(person));
        Assert.assertEquals(root, person);
    }
}
