package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;


public class _FileCrawlerTest {

    @Test
    public void testExecute() {
        String rootDirPath = getClass().getResource("dir1").getFile();
        FileCrawler crawler = new FileCrawler(rootDirPath, 10);
        FileCrawlerResults results = crawler.execute();
        assertEquals(2, results.getProcessedFiles().size());

        Map<String, ? extends Number> statistics = results.getWordStatistics();
        assertEquals(19, results.getWordStatistics().size());
        assertEquals(3, statistics.get("съешь").intValue());
        assertEquals(3, statistics.get("ещё").intValue());
        assertEquals(3, statistics.get("этих").intValue());
        assertEquals(3, statistics.get("мягких").intValue());
        assertEquals(3, statistics.get("французских").intValue());
        assertEquals(3, statistics.get("булок").intValue());
        assertEquals(3, statistics.get("да").intValue());
        assertEquals(3, statistics.get("выпей").intValue());
        assertEquals(3, statistics.get("чаю").intValue());
        assertEquals(1, statistics.get("3333").intValue());
        assertEquals(2, statistics.get("три").intValue());
        assertEquals(1, statistics.get("тысячи").intValue());
        assertEquals(1, statistics.get("триста").intValue());
        assertEquals(1, statistics.get("тридцать").intValue());
        assertEquals(3, statistics.get("three").intValue());
        assertEquals(1, statistics.get("thousand").intValue());
        assertEquals(1, statistics.get("hundred").intValue());
        assertEquals(1, statistics.get("and").intValue());
        assertEquals(1, statistics.get("thirty").intValue());
    }
    @Test
    public void testExecuteWithComplexDelimiter() {
        String rootDirPath = getClass().getResource("dir2").getFile();
        FileCrawler crawler = new FileCrawler(rootDirPath, 10);
        FileCrawlerResults results = crawler.execute();

        assertEquals(1, results.getProcessedFiles().size());

        Map<String, ? extends Number> statistics = results.getWordStatistics();
        assertEquals(3, results.getWordStatistics().size());
        assertEquals(1, statistics.get("Арам").intValue());
        assertEquals(1, statistics.get("asd").intValue());
        assertEquals(1, statistics.get("123юю").intValue());
    }

    @Test
    public void testExecuteCaseSensitive() {
        String rootDirPath = getClass().getResource("dir3").getFile();
        FileCrawler crawler = new FileCrawler(rootDirPath, 10);
        FileCrawlerResults results = crawler.execute();

        assertEquals(1, results.getProcessedFiles().size());

        Map<String, ? extends Number> statistics = results.getWordStatistics();
        assertEquals(8, results.getWordStatistics().size());
        assertEquals(1, statistics.get("Кот").intValue());
        assertEquals(1, statistics.get("кот").intValue());
        assertEquals(1, statistics.get("КОТ").intValue());
        assertEquals(1, statistics.get("кОт").intValue());
        assertEquals(1, statistics.get("коТ").intValue());
        assertEquals(1, statistics.get("КОт").intValue());
        assertEquals(1, statistics.get("кОТ").intValue());
        assertEquals(1, statistics.get("кoт").intValue());
    }
}
