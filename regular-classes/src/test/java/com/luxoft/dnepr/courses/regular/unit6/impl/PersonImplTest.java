package com.luxoft.dnepr.courses.regular.unit6.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import junit.framework.Assert;
import org.junit.Test;

import java.io.*;

public class PersonImplTest {
    private PersonImpl father = new PersonImpl("Jack", "canadian", null, null, Gender.MALE, 54);
    private PersonImpl mother = new PersonImpl("Marine", "armenian", null, null, Gender.FEMALE, 51);
    private PersonImpl person = new PersonImpl("Sam", "german", father, mother, Gender.MALE, 25);
    private String fileName = "person.txt";

    @Test
    public void testSerialize() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
        os.writeObject(person);
    }

    @Test
    public void testDeserialize() throws IOException, ClassNotFoundException {
        testSerialize();
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        PersonImpl root = (PersonImpl) in.readObject();
        Assert.assertEquals(person, root);
    }
}
