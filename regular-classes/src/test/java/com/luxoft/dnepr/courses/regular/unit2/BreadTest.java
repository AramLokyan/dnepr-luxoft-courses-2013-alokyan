package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class BreadTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws CloneNotSupportedException {
        Bread bread = productFactory.createBread("br", "white", 10, 1.0);
        Bread cloned = (Bread) bread.clone();
        assertTrue(bread.equals(cloned));
        assertEquals(cloned.getCode(), bread.getCode());
        assertEquals(cloned.getName(), bread.getName());
        assertEquals(cloned.getPrice(), bread.getPrice(), 0);
        assertEquals(cloned.getWeight(), bread.getWeight(), 0);
        if (cloned.hashCode() != bread.hashCode()) {
            fail("Bread.clone() Hash code have to be the same.");
        }
        if (cloned == bread) {
            fail("Bread.clone() The clone has to be another instance.");
        }
    }

    @Test
    public void testEquals() throws CloneNotSupportedException {
        Bread bread = productFactory.createBread("br", "white", 10, 1.0);
        Bread cloned = productFactory.createBread("br", "white", 10, 1.0);
        assertTrue(bread.equals(cloned));
        assertEquals(cloned.getCode(), bread.getCode());
        assertEquals(cloned.getName(), bread.getName());
        assertEquals(cloned.getPrice(), bread.getPrice(), 0);
        assertEquals(cloned.getWeight(), bread.getWeight(), 0);
    }

    @Test
    public void testEqualsError() throws CloneNotSupportedException {
        Bread bread = new Bread();
        Bread cloned = new Bread();
        assertTrue(bread.equals(cloned));
        assertEquals(cloned.getCode(), bread.getCode());
        assertEquals(cloned.getName(), bread.getName());
        assertEquals(cloned.getPrice(), bread.getPrice(), 0);
        assertEquals(cloned.getWeight(), bread.getWeight(), 0);
    }

    @Test
    public void testCloneError() throws CloneNotSupportedException {
        Bread bread = new Bread();
        Bread cloned = (Bread) bread.clone();
        if (cloned.hashCode() != bread.hashCode()) {
            fail("Book.clone() Hash code have to be the same.");
        }
        if (cloned == bread) {
            fail("Book.clone() The clone has to be another instance.");
        }
        assertTrue(bread.equals(cloned));
        assertEquals(cloned.getCode(), bread.getCode());
        assertEquals(cloned.getName(), bread.getName());
        assertEquals(cloned.getPrice(), bread.getPrice(), 0);
        assertEquals(cloned.getWeight(), bread.getWeight(), 0);
    }

    @Test
    public void testGetPrice() {
        Bread bread = new Bread("br", "white", 10, 1.0);
        assertEquals(10, bread.getPrice(), 0);
    }

    @Test
    public void testGetPriceError() {
        Bread bread = new Bread("br", "white", -10, 1.0);
        assertEquals(10, bread.getPrice(), 0);
    }
}
