package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Date;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 09.11.13
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
public class Car {
    private String type;
    private int price;
    private Date dayOfTrade;

    public Car() {
    }

    public Car(String type, int price, Date dayOfTrade) {
        this.type = type;
        this.price = price;
        this.dayOfTrade = dayOfTrade;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getDayOfTrade() {
        return dayOfTrade;
    }

    public void setDayOfTrade(Date dayOfTrade) {
        this.dayOfTrade = dayOfTrade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (price != car.price) return false;
        if (dayOfTrade != null ? !dayOfTrade.equals(car.dayOfTrade) : car.dayOfTrade != null) return false;
        if (type != null ? !type.equals(car.type) : car.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return new Random().nextInt();
    }

    @Override
    public String toString() {
        return "Car{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", dayOfTrade=" + dayOfTrade +
                '}';
    }
}
