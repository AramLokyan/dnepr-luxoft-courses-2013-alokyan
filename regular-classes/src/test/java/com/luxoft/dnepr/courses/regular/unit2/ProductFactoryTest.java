package com.luxoft.dnepr.courses.regular.unit2;


import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;

public class ProductFactoryTest {

    @Test
    public void testCreateBread() {
        ProductFactory factory = new ProductFactory();
        Bread bread = factory.createBread("br", "white", 10, 1.0);
        Bread clone = new Bread("br", "white", 10, 1.0);
        Assert.assertTrue(bread.equals(clone));
        Assert.assertNotSame(clone, bread);

        clone = factory.createBread("br", "white", 10, 1.0);
        Assert.assertTrue(bread.equals(clone));
        Assert.assertNotSame(clone, bread);

    }

    @Test
    public void testCreateBreadError() {
        ProductFactory factory = new ProductFactory();
        Bread bread = factory.createBread(null, null, 0, -1);
        Bread clone = new Bread(null, null, 0, -1);
        Assert.assertTrue(bread.equals(clone));
        Assert.assertNotSame(clone, bread);

        clone = factory.createBread(null, null, 0, -1);
        Assert.assertTrue(bread.equals(clone));
        Assert.assertNotSame(clone, bread);
    }

    @Test
     public void testCreateBook() {
        ProductFactory factory = new ProductFactory();
        Book book = factory.createBook("book", "Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Book clone = new Book("book", "Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertTrue(book.equals(clone));
        Assert.assertNotSame(clone, book);

        clone = factory.createBook("book", "Java", 100, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertTrue(book.equals(clone));
        Assert.assertNotSame(clone, book);

    }

    @Test
    public void testCreateBookError() {
        ProductFactory factory = new ProductFactory();
        Book book = factory.createBook(null, null, 100, null);
        Book clone = new Book(null, null, 0, null);
        Assert.assertTrue(book.equals(clone));
        Assert.assertNotSame(clone, book);

        clone = factory.createBook(null, null, 100, null);
        Assert.assertTrue(book.equals(clone));
        Assert.assertNotSame(clone, book);
    }

    @Test
    public void testCreateBeverage() {
        ProductFactory factory = new ProductFactory();
        Beverage drink = factory.createBeverage("cola", "Coca-cola", 100, true);
        Beverage clone = new Beverage("cola", "Coca-cola", 100, true);
        Assert.assertTrue(drink.equals(clone));
        Assert.assertNotSame(clone, drink);

        clone = factory.createBeverage("cola", "Coca-cola", 100, true);
        Assert.assertTrue(drink.equals(clone));
        Assert.assertNotSame(clone, drink);

    }

    @Test
    public void testCreateBeverageError() {
        ProductFactory factory = new ProductFactory();
        Beverage drink = factory.createBeverage(null, null, 100, false);
        Beverage clone = new Beverage(null, null, 0, false);
        Assert.assertTrue(drink.equals(clone));
        Assert.assertNotSame(clone, drink);

        clone = factory.createBeverage(null, null, 100, false);
        Assert.assertTrue(drink.equals(clone));
        Assert.assertNotSame(clone, drink);
    }

}
