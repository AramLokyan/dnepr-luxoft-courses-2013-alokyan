package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 05.11.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {

    @Test
    public void testCheckWithdrawal() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkWithdrawal(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Wallet has enough funds for operation.");
        }
    }

    @Test
    public void testCheckWithdrawalError() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.BLOCKED, new BigDecimal(250));
        try {
            wallet.checkWithdrawal(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            Assert.assertTrue(e.getMessage().startsWith("Wallet.checkWithdrawal() validation failed."));
            Assert.assertTrue(e.getMessage().endsWith("Wallet is blocked."));
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        }
        wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkWithdrawal(new BigDecimal(200));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.assertTrue(e.getMessage().contains("Not enough funds to perform this operation"));
        }
        try {
            wallet.checkWithdrawal(null);
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(e.getMessage().endsWith("amountToWithdraw is null or incorrect"));
        }
        try {
            wallet.checkWithdrawal(new BigDecimal(-200));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(e.getMessage().endsWith("amountToWithdraw is null or incorrect"));
        }

        wallet = new Wallet(1L, new BigDecimal(100.12345), null, new BigDecimal(250));
        try {
            wallet.checkWithdrawal(new BigDecimal(5));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().endsWith("Wallet state is incorrect, some fields are null."));
        }
        wallet = new Wallet(1L, null, WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkWithdrawal(new BigDecimal(5));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().endsWith("Wallet state is incorrect, some fields are null."));
        }
        wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, null);
        try {
            wallet.checkWithdrawal(new BigDecimal(5));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Never happens");
        } catch (InsufficientWalletAmountException e) {
            Assert.fail("Never happens");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().endsWith("Wallet state is incorrect, some fields are null."));
        }

    }

    @Test
    public void testCheckTransfer() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkTransfer(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        }
    }

    @Test
    public void testCheckTransferError() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.BLOCKED, new BigDecimal(250));
        try {
            wallet.checkTransfer(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            Assert.assertTrue(e.getMessage().endsWith("Wallet is blocked."));
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        }
        wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkTransfer(new BigDecimal(150));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.assertTrue(e.getMessage().contains("Max wallet funds exceeded."));
        }
        try {
            wallet.checkTransfer(null);
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(e.getMessage().endsWith("amountToTransfer is null or incorrect"));
        }
        try {
            wallet.checkTransfer(new BigDecimal(-1));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(e.getMessage().endsWith("amountToTransfer is null or incorrect"));
        }
        wallet = new Wallet(1L, null, WalletStatus.ACTIVE, new BigDecimal(250));
        try {
            wallet.checkTransfer(new BigDecimal(150));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().contains("Wallet state is incorrect, some fields are null."));
        }
        wallet = new Wallet(1L, new BigDecimal(100.12345), null, new BigDecimal(250));
        try {
            wallet.checkTransfer(new BigDecimal(150));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().contains("Wallet state is incorrect, some fields are null."));
        }
        wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, null);
        try {
            wallet.checkTransfer(new BigDecimal(150));
        } catch (WalletIsBlockedException e) {
            Assert.fail("Wallet can't be BLOCKED in here.");
        } catch (LimitExceededException e) {
            Assert.fail("Wallet has correct limit to perform this operation.");
        } catch (IllegalStateException e) {
            Assert.assertTrue(e.getMessage().contains("Wallet state is incorrect, some fields are null."));
        }

    }

    @Test
    public void testWithdrawal() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        wallet.withdraw(new BigDecimal(50));
        Assert.assertEquals(100.12345 - 50, wallet.getAmount().doubleValue(), 0.01);

    }

    @Test
    public void testTransfer() {
        Wallet wallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(250));
        wallet.transfer(new BigDecimal(50));
        Assert.assertEquals(100.12345 + 50, wallet.getAmount().doubleValue(), 0.01);
    }

}
