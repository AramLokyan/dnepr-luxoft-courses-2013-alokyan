package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BillTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAppend() throws Exception {
        Bill bill = new Bill();
        //append a piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append the same piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append a beverage
        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        assertEquals(2, bill.getProducts().size());

        //append another beverage
        bill.append(factory.createBeverage("bev2", "Martini", 100, false));
        assertEquals(3, bill.getProducts().size());
    }

    @Test
    public void testAppendError() throws Exception {
        Bill bill = new Bill();
        //append a piece of bread
        bill.append(factory.createBread("", "", 0, 0));
        assertEquals(1, bill.getProducts().size());

        //append the same piece of bread
        bill.append(factory.createBread(null, null, 0, 0));
        assertEquals(2, bill.getProducts().size());

        //append a beverage
        bill.append(factory.createBeverage("", "", 0, true));
        assertEquals(3, bill.getProducts().size());

        //append another beverage
        bill.append(factory.createBeverage(null, null, 0, true));
        assertEquals(4, bill.getProducts().size());

        //append a book
        bill.append(factory.createBook("", "", 0, null));
        assertEquals(5, bill.getProducts().size());

        //append another book
        bill.append(factory.createBook(null, null, 0, null));
        assertEquals(6, bill.getProducts().size());
    }

    @Test
    public void testSummarize() throws Exception {
        Bill bill = new Bill();
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        Beverage cola = factory.createBeverage("bev", "Cola", 10, true);
        bill.append(cola);
        bill.append(cola/*.clone()*/);
        Beverage colaExpensive = factory.createBeverage("bev", "Cola", 50, true);
        bill.append(colaExpensive);

        bill.append(factory.createBook("book", "Java", 100, new Date()));
        assertEquals((10 + 10) * 0.95 + (10 + 10 + 50) * 0.9 + 100, bill.summarize(), 0);
    }

    @Test
    public void testSummarizeError() throws Exception {
        Bill bill = new Bill();
        bill.append(factory.createBread("", "", 0, 0));
        assertEquals(1, bill.getProducts().size());

        //append the same piece of bread
        bill.append(factory.createBread(null, null, 0, 0));
        assertEquals(2, bill.getProducts().size());

        //append a beverage
        bill.append(factory.createBeverage("", "", 0, true));
        assertEquals(3, bill.getProducts().size());

        //append another beverage
        bill.append(factory.createBeverage(null, null, 0, true));
        assertEquals(4, bill.getProducts().size());

        //append a book
        bill.append(factory.createBook("", "", 0, null));
        assertEquals(5, bill.getProducts().size());

        //append another book
        bill.append(factory.createBook(null, null, 0, null));
        assertEquals(6, bill.getProducts().size());

        assertEquals(0, bill.summarize(), 0);


    }

    @Test
    public void testGetProducts() throws Exception {
        Bill bill = new Bill();

        assertEquals(0, bill.getProducts().size());

        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBook("book", "Linux", 200, new Date()));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        bill.append(factory.createBeverage("bev", "Cola", 12, true));

        bill.append(factory.createBook("book", "Java", 100, new Date()));

        List<Product> groupedAndSortedProducts = bill.getProducts();
        assertEquals(4, groupedAndSortedProducts.size());


        //test sorting - by grouped price, descending
        assertTrue(
                groupedAndSortedProducts.get(0).getPrice() >= groupedAndSortedProducts.get(1).getPrice() &&
                        groupedAndSortedProducts.get(1).getPrice() >= groupedAndSortedProducts.get(2).getPrice() &&
                        groupedAndSortedProducts.get(2).getPrice() >= groupedAndSortedProducts.get(3).getPrice()
        );

        assertEquals("Linux", groupedAndSortedProducts.get(0).getName());
        assertEquals("Java", groupedAndSortedProducts.get(1).getName());
        assertEquals("Bread", groupedAndSortedProducts.get(2).getName());
        assertEquals("Cola", groupedAndSortedProducts.get(3).getName());
    }

    @Test
    public void testGetProductsError() throws Exception {
        Bill bill = new Bill();

        List<Product> emptyProductList = bill.getProducts();
        assertEquals(0, emptyProductList.size());
    }
}
