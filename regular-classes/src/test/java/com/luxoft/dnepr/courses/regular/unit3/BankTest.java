package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 05.11.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {
    private Bank myBank = new Bank(System.getProperty("java.version"));
    private Map<Long, UserInterface> users = new HashMap<>();

    @Before
    public void bankFactory() {
/*1*/   Wallet johnWallet = new Wallet(1L, new BigDecimal(100.12345), WalletStatus.ACTIVE, new BigDecimal(150));
        User john = new User(1L, "John", johnWallet);

/*2*/   Wallet samWallet = new Wallet(2L, new BigDecimal(200.3456), WalletStatus.ACTIVE, new BigDecimal(300));
        User sam = new User(2L, "Sam", samWallet);

/*3*/   Wallet sarahWallet = new Wallet(3L, new BigDecimal(300.7689), WalletStatus.BLOCKED, new BigDecimal(450));
        User sarah = new User(3L, "Sarah", sarahWallet);

/*4*/   Wallet mikeWallet = new Wallet(4L, new BigDecimal(400.0001), WalletStatus.BLOCKED, new BigDecimal(600));
        User mike = new User(4L, "Mike", mikeWallet);

        users.put(john.getId(), john);
        users.put(sam.getId(), sam);
        users.put(sarah.getId(), sarah);
        users.put(mike.getId(), mike);

        myBank.setUsers(users);
    }

    @Test
    public void testBank() {
        try {
            new Bank(System.getProperty("java.version"));
        } catch (IllegalJavaVersionError e) {
            Assert.fail("testBank() Never happens");
        } catch (Throwable e) {
            Assert.fail("testBank() Uncaught exception/error: " + e.getMessage());
        }
    }

    @Test
    public void testBankError() {
        try {
            try {
                new Bank(null);
                Assert.fail("testBankError() Expected Java Version is null.");
            } catch (IllegalJavaVersionError e) { /* ignore */ }
            try {
                new Bank("");
                Assert.fail("testBankError() Expected Java Version is empty.");
            } catch (IllegalJavaVersionError e) { /* ignore */ }
            try {
                new Bank("1.2");
                Assert.fail("testBankError() Illegal Argument Exception. Expected Java Version is incorrect.");
            } catch (IllegalJavaVersionError e) { /* ignore */ }

        } catch (Throwable e) {
            Assert.fail("testBankError() Uncaught exception/error: " + e.getMessage());
        }

    }

    @Test
    public void testMakeMoneyTransaction() {
        try {
            myBank.makeMoneyTransaction(2L, 1L, new BigDecimal(49.15));
            Assert.assertEquals(200.3456-49.15, users.get(2L).getWallet().getAmount().doubleValue(), 0.01);
            Assert.assertEquals(100.12345+49.15, users.get(1L).getWallet().getAmount().doubleValue(), 0.01);
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens, but " + e.getMessage());
        } catch (TransactionException e) {
            Assert.fail("Never happens, but " + e.getMessage());
        }
    }

    @Test
    public void testMakeMoneyTransactionError(){
        try {
            myBank.makeMoneyTransaction(0L, 1L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.assertTrue(e.getMessage().startsWith("No such user found, id = "));
        } catch (TransactionException e) {
            Assert.fail("Never happens");
        }
        try {
            myBank.makeMoneyTransaction(1L, 0L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.assertTrue(e.getMessage().startsWith("No such user found, id = "));
        } catch (TransactionException e) {
            Assert.fail("Never happens");
        }
        try {
            myBank.makeMoneyTransaction(1L, 1L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("Transaction conditions are not valid."));
        }
        try {
            myBank.makeMoneyTransaction(1L, 1L, new BigDecimal(-150.00));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("Transaction conditions are not valid."));
        }
        try {
            myBank.makeMoneyTransaction(2L, 1L, null);
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("Transaction conditions are not valid."));
        }
        try {
            myBank.makeMoneyTransaction(2L, 1L, new BigDecimal(0));
            Assert.fail("Zero money transaction.");
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens, but " + e.getMessage());
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("Transaction conditions are not valid."));
        }
        try {
            myBank.makeMoneyTransaction(3L, 4L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("User"));
            Assert.assertTrue(e.getMessage().endsWith("wallet is blocked"));
        }
        try {
            myBank.makeMoneyTransaction(1L, 4L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("User"));
            Assert.assertTrue(e.getMessage().endsWith("wallet is blocked"));
        }
        try {
            myBank.makeMoneyTransaction(2L, 1L, new BigDecimal(150));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("User"));
            Assert.assertTrue(e.getMessage().contains("wallet limit exceeded"));
        }
        try {
            myBank.makeMoneyTransaction(2L, 1L, new BigDecimal(300));
        } catch (NoUserFoundException e) {
            Assert.fail("Never happens");
        } catch (TransactionException e) {
            Assert.assertTrue(e.getMessage().startsWith("User"));
            Assert.assertTrue(e.getMessage().contains("has insufficient funds"));
        }



    }
}
