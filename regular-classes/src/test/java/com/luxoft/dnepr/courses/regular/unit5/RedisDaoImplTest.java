package com.luxoft.dnepr.courses.regular.unit5;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

public class RedisDaoImplTest {
    @Before
    public void storageClear() {
        EntityStorage.getEntities().clear();
    }

    @Test
    public void testRedisSave() {
        IDao<Redis> redisIDao = new RedisDaoImpl();

        redisIDao.save(new Redis(null, 19));
        redisIDao.save(new Redis(40L, 19));
        redisIDao.save(new Redis(null, 20));
        redisIDao.save(new Redis(21));
        redisIDao.save(new Redis());

        Map<Long, Entity> storage = EntityStorage.getEntities();
        Assert.assertTrue(storage.size()==5);
        Assert.assertTrue(storage.keySet().containsAll(Arrays.asList(new Long[]{1L, 40L, 41L, 42L, 43L})));
    }

    @Test
    public void testRedisSaveError() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(1L, 19));

        try {
            redisIDao.save(new Redis(1L, 19));
            Assert.fail("User with id = 1 already exist.");
        } catch (UserAlreadyExist e) {
            Assert.assertTrue(e.getMessage().endsWith("already exists."));
        }

        try{
            redisIDao.save(null);
            Assert.fail("User is null.");
        } catch (IllegalArgumentException e){
            Assert.assertTrue(e.getMessage().endsWith("Entity is null"));
        }
    }

    @Test
    public void testRedisUpdate() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        redisIDao.update(new Redis(8L, 21));

        Map<Long, Entity> storage = EntityStorage.getEntities();
        Redis redis = (Redis) storage.get(8L);
        Assert.assertTrue(redis.getWeight() == 21);
    }

    @Test
    public void testRedisUpdateError() {
        IDao<Redis> redisIDao = new RedisDaoImpl();

        try {
            redisIDao.update(new Redis(null, 21));
            Assert.fail("User with id = null is not allowed.");
        } catch (UserNotFound e) {
            Assert.assertTrue(e.getMessage().endsWith("is not found."));
        }

        try {
            redisIDao.update(new Redis(6L, 21));
            Assert.fail("User with id = 6 is not found.");
        } catch (UserNotFound e) {
            Assert.assertTrue(e.getMessage().endsWith("is not found."));
        }
    }

    @Test
    public void testRedisUpdateUsageError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));

        IDao<Redis> redisIDao = new RedisDaoImpl();
        Assert.assertEquals(null, redisIDao.update(new Redis(3L, 21)));

        Assert.assertTrue(employeeIDao.get(3L).getSalary() == 12500);
    }

    @Test
    public void testRedisGet() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        Redis redis = redisIDao.get(8L);
        Assert.assertEquals(redis.getWeight(), 20);
    }

    @Test
    public void testRedisGetError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(1L, 12000));

        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        Redis redis = redisIDao.get(6L);
        Assert.assertTrue(redis == null);

        redis = redisIDao.get(1L);
        Assert.assertTrue(redis == null);
    }

    @Test
    public void testRedisDelete() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        Assert.assertTrue(redisIDao.delete(8L));
    }

    @Test
    public void testRedisDeleteError() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        Assert.assertFalse(redisIDao.delete(4L));

        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(1L, 12000));
        Assert.assertFalse(redisIDao.delete(1L));
    }
}
