package com.luxoft.dnepr.courses.regular.unit5;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

public class EmployeeDaoImplTest {
    @Before
    public void storageClear() {
        EntityStorage.getEntities().clear();
    }

    @Test
    public void testEmployeeSave() {
        IDao<Employee> EmployeeIDao = new EmployeeDaoImpl();
        EmployeeIDao.save(new Employee(null, 1200));
        EmployeeIDao.save(new Employee(40L, 1200));
        EmployeeIDao.save(new Employee(null, 1300));
        EmployeeIDao.save(new Employee(1400));
        EmployeeIDao.save(new Employee());

        Map<Long, Entity> storage = EntityStorage.getEntities();
        Assert.assertTrue(storage.size()==5);
        Assert.assertTrue(storage.keySet().containsAll(Arrays.asList(new Long[]{1L, 40L, 41L, 42L, 43L})));
    }

    @Test
    public void testEmployeeSaveError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(1L, 12000));

        try {
            employeeIDao.save(new Employee(1L, 19));
            Assert.fail("User with id = 1 already exist.");
        } catch (UserAlreadyExist e) {
            Assert.assertTrue(e.getMessage().endsWith("already exists."));
        }

        try{
            employeeIDao.save(null);
            Assert.fail("User is null.");
        } catch (IllegalArgumentException e){
            Assert.assertTrue(e.getMessage().endsWith("Entity is null"));
        }
    }

    @Test
    public void testEmployeeUpdate() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));
        employeeIDao.update(new Employee(3L, 17000));

        Map<Long, Entity> storage = EntityStorage.getEntities();
        Employee employee = (Employee) storage.get(3L);
        Assert.assertTrue(employee.getSalary() == 17000);
    }

    @Test
    public void testEmployeeUpdateError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();

        try {
            employeeIDao.update(new Employee(6L, 17000));
            Assert.fail("User with id = 6 is not exist.");
        } catch (UserNotFound e) {
            Assert.assertTrue(e.getMessage().endsWith("is not found."));
        }

        try {
            employeeIDao.update(new Employee(null, 17000));
            Assert.fail("User with id = null is not allowed.");
        } catch (UserNotFound e) {
            Assert.assertTrue(e.getMessage().endsWith("is not found."));
        }
    }

    @Test
    public void testEmployeeUpdateUsageError() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));

        IDao<Employee> employeeIDao = new EmployeeDaoImpl();

        Assert.assertEquals(null, employeeIDao.update(new Employee(8L, 12500)));
        Assert.assertTrue(redisIDao.get(8L).getWeight() == 20);
    }

    @Test
    public void testEmployeeGet() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));
        Employee employee = employeeIDao.get(3L);
        Assert.assertEquals(employee.getSalary(), 12500);
    }

    @Test
    public void testEmployeeGetError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));
        Employee employee = employeeIDao.get(8L);
        Assert.assertTrue(employee == null);

        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        employee = employeeIDao.get(8L);
        Assert.assertTrue(employee == null);
    }

    @Test
    public void testEmployeeDelete() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));
        Assert.assertTrue(employeeIDao.delete(3L));
    }

    @Test
    public void testEmployeeDeleteError() {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        employeeIDao.save(new Employee(3L, 12500));
        Assert.assertFalse(employeeIDao.delete(4L));

        IDao<Redis> redisIDao = new RedisDaoImpl();
        redisIDao.save(new Redis(8L, 20));
        Assert.assertFalse(employeeIDao.delete(8L));
    }
}
