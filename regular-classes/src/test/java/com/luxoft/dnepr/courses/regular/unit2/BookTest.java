package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        assertTrue(book.equals(cloned));
        assertEquals(cloned.getCode(), book.getCode());
        assertEquals(cloned.getName(), book.getName());
        assertEquals(cloned.getPrice(), book.getPrice(), 0);
        assertEquals(cloned.getPublicationDate(), book.getPublicationDate());
        assertTrue(cloned.getPublicationDate()!=book.getPublicationDate());
        if (cloned.hashCode() != book.hashCode()) {
            fail("Book.clone() Hash code have to be the same.");
        }
        if (cloned == book) {
            fail("Book.clone() The clone has to be another instance.");
        }
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertTrue(book.equals(cloned));
        assertEquals(cloned.getCode(), book.getCode());
        assertEquals(cloned.getName(), book.getName());
        assertEquals(cloned.getPrice(), book.getPrice(), 0);
        assertEquals(cloned.getPublicationDate(), book.getPublicationDate());
    }

    @Test
    public void testEqualsError() throws CloneNotSupportedException {
        Book book = new Book();
        Book cloned = new Book();
        assertTrue(book.equals(cloned));
        assertEquals(cloned.getCode(), book.getCode());
        assertEquals(cloned.getName(), book.getName());
        assertEquals(cloned.getPrice(), book.getPrice(), 0);
        assertEquals(cloned.getPublicationDate(), book.getPublicationDate());
    }

    @Test
    public void testCloneError() throws CloneNotSupportedException {
        Book book = new Book();
        Book cloned = (Book) book.clone();
        if (cloned.hashCode() != book.hashCode()) {
            fail("Book.clone() Hash code have to be the same.");
        }
        if (cloned == book) {
            fail("Book.clone() The clone has to be another instance.");
        }
        assertTrue(book.equals(cloned));
        assertEquals(cloned.getCode(), book.getCode());
        assertEquals(cloned.getName(), book.getName());
        assertEquals(cloned.getPrice(), book.getPrice(), 0);
        assertEquals(cloned.getPublicationDate(), book.getPublicationDate());

        // null == null
        assertTrue(cloned.getPublicationDate()==book.getPublicationDate());
    }

    @Test
    public void testGetPrice() {
        Book book = new Book("book", "Java", 10, new Date());
        assertEquals(10, book.getPrice(), 0);
    }

    @Test
    public void testGetPriceError() {
        Book book = new Book("book", "Java", -10, new Date());
        assertEquals(10, book.getPrice(), 0);
    }
}
