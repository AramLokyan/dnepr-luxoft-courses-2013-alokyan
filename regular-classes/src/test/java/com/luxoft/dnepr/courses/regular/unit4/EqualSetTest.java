package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 09.11.13
 * Time: 18:51
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {
    private final Human john = new Human("John", 23, new GregorianCalendar(1987, 7, 7).getTime());
    private final Human sam = new Human("John", 23, new GregorianCalendar(1987, 7, 7).getTime());
    private final Car bmw = new Car("John", 23, new GregorianCalendar(1987, 7, 7).getTime());
    private final Car lada = new Car("John", 23, new GregorianCalendar(1987, 7, 7).getTime());

    @Test
    public void testConstructor() {
        Set defaultConstructor = new EqualSet();
        Assert.assertEquals(0, defaultConstructor.size());
        Assert.assertTrue(defaultConstructor.isEmpty());

        List list = new ArrayList<>();
        list.add(john);
        list.add(sam);
        list.add(bmw);
        list.add(lada);
        list.add(null);
        list.add(null);
        Set additionConstructor = new EqualSet(list);
        Assert.assertEquals(list.size() - 3, additionConstructor.size());
        Assert.assertTrue(additionConstructor.containsAll(list));
    }

    @Test
    public void testSize() {
        Set objectSet = new EqualSet();
        Assert.assertEquals(0, objectSet.size());
        objectSet.add("first element");
        objectSet.add("second element");
        Assert.assertEquals(2, objectSet.size());
        objectSet.add("second element");
        objectSet.add("second element");
        objectSet.add("second element");
        Assert.assertEquals(2, objectSet.size());
        objectSet.add(null);
        Assert.assertEquals(3, objectSet.size());

        Set<Human> genericSet = new EqualSet<>();
        Assert.assertEquals(0, genericSet.size());
        genericSet.add(john);
        genericSet.add(sam);
        Assert.assertEquals(1, genericSet.size());
        genericSet.add(new Human("John", 23, new GregorianCalendar(1987, 7, 7).getTime()));
        genericSet.add(new Human("John", 23, new GregorianCalendar(1987, 7, 7).getTime()));
        Assert.assertEquals(1, genericSet.size());
        genericSet.add(null);
        Assert.assertEquals(2, genericSet.size());
    }

    @Test
    public void testIsEmpty() {
        Set objectSet = new EqualSet();

        Assert.assertTrue(objectSet.isEmpty());
        objectSet.add("element");
        Assert.assertFalse(objectSet.isEmpty());

        objectSet = new EqualSet();
        Assert.assertTrue(objectSet.isEmpty());
        objectSet.add(null);
        objectSet.add(null);
        objectSet.add("element");

        Assert.assertFalse(objectSet.isEmpty());
        objectSet.remove(null);
        objectSet.remove("element");
        Assert.assertTrue(objectSet.isEmpty());
    }

    @Test
    public void testContains() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add("second");
        objectSet.add(null);
        objectSet.add(3);

        Assert.assertTrue(objectSet.contains("first"));
        Assert.assertTrue(objectSet.contains(3));
        Assert.assertTrue(objectSet.contains("second"));
        Assert.assertTrue(objectSet.contains(null));

        Assert.assertFalse(objectSet.contains(6));
        Assert.assertFalse(objectSet.contains(""));

        objectSet = new EqualSet();

        Assert.assertTrue(objectSet.add(john));
        Assert.assertTrue(objectSet.add(bmw));
        Assert.assertFalse(objectSet.add(sam));
        Assert.assertFalse(objectSet.add(lada));

        Assert.assertTrue(objectSet.contains(sam));
        Assert.assertTrue(objectSet.contains(lada));
        Assert.assertEquals(2, objectSet.size());
    }

    @Test
    public void testAdd() {
        Set objectSet = new EqualSet();

        Assert.assertTrue(objectSet.add(john));
        Assert.assertFalse(objectSet.add(sam));

        Assert.assertTrue(objectSet.add(bmw));
        Assert.assertFalse(objectSet.add(lada));

        Assert.assertEquals(2, objectSet.size());
    }

    @Test
    public void testRemove() {
        Set objectSet = new EqualSet();

        objectSet.add(john);
        objectSet.add(bmw);
        Assert.assertEquals(2, objectSet.size());

        Assert.assertTrue(objectSet.remove(sam));
        Assert.assertEquals(1, objectSet.size());
        Assert.assertEquals(objectSet.iterator().next(), lada);
        Assert.assertTrue(objectSet.remove(bmw));
        Assert.assertFalse(objectSet.remove(bmw));
    }

    @Test
    public void testContainsAll() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add("Java");

        List list = new ArrayList<>();
        list.add("first");
        list.add("Java");
        list.add(null);
        Assert.assertTrue(objectSet.containsAll(list));

        list.add(null);
        list.add("Java");
        Assert.assertTrue(objectSet.containsAll(list));

        list.add("second");
        Assert.assertFalse(objectSet.containsAll(list));
    }

    @Test
    public void testAddAll() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add("Java");
        List list = new ArrayList<>();
        list.add("first");
        list.add("Java");
        list.add(null);

        Assert.assertFalse(objectSet.addAll(list));
        list.add("second");

        Assert.assertTrue(objectSet.addAll(list));
        Assert.assertEquals(6, objectSet.size());
    }

    @Test
    public void testRetainAll() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add("Java");

        List list = new ArrayList<>();
        list.add("first");
        list.add("first");
        list.add("Java");
        list.add("Java");
        list.add(null);
        list.add(null);

        Assert.assertTrue(objectSet.retainAll(list));
        Assert.assertArrayEquals(new Object[]{"first", null, "Java"}, objectSet.toArray());

        list.add("OOO");
        list.add("OOO");
        Assert.assertFalse(objectSet.retainAll(list));

        list = Arrays.asList(new Integer[]{7, 56, 2346, 8, 3, 2, 12});
        Assert.assertTrue(objectSet.retainAll(list));
        Assert.assertTrue(objectSet.isEmpty());
    }

    @Test
    public void testRetainAll2() {
        Set<Integer> set = new EqualSet<>();
        set.addAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 0, 16, 27, 68}));

        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 6, 7, 8}));

        Assert.assertArrayEquals(new Integer[]{1, 2, 3, 4, 5}, set.toArray());

        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 4}));
        Assert.assertArrayEquals(new Integer[]{1, 2, 3, 4}, set.toArray());

        set.add(5);
        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 7}));
        Assert.assertArrayEquals(new Integer[]{1, 2, 3}, set.toArray());
    }

    @Test
    public void testRemoveAll() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add("Java");
        List list = new ArrayList<>();
        list.add("first");
        list.add("Java");
        list.add("Java");
        list.add(null);

        Assert.assertTrue(objectSet.removeAll(list));
        Assert.assertEquals(2, objectSet.size());

        Assert.assertTrue(objectSet.contains(3));
        Assert.assertTrue(objectSet.contains(3.14));
    }

    @Test
    public void testToArray() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add("Java");

        Object[] expected = new Object[]{"first", null, 3, 3.14, "Java"};
        Object[] actual = objectSet.toArray();
        Assert.assertEquals(actual.length, expected.length);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testToArrayGeneric() {
        Set<String> genericSet = new EqualSet<>();
        genericSet.add("first");
        genericSet.add(null);
        genericSet.add("");
        genericSet.add("3.14");
        genericSet.add("Java");

        String[] expected = new String[]{"first", null, "", "3.14", "Java"};

        String[] actual = genericSet.toArray(new String[0]);
        Assert.assertEquals(actual.length, expected.length);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testClear() {
        Set objectSet = new EqualSet();
        objectSet.add(john);
        objectSet.add(bmw);
        objectSet.add(null);
        objectSet.add("");
        objectSet.add(123.456);
        Assert.assertEquals(5, objectSet.size());
        objectSet.clear();
        Assert.assertEquals(0, objectSet.size());
        Assert.assertTrue(objectSet.isEmpty());
    }

    @Test
    public void testIterator() {
        Set objectSet = new EqualSet();
        objectSet.add("first");
        objectSet.add(null);
        objectSet.add(3);
        objectSet.add(3.14);
        objectSet.add(john);
        objectSet.add(null);

        Iterator iterator = objectSet.iterator();
        int size = objectSet.size();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            Assert.assertTrue(objectSet.contains(next));
            Assert.assertEquals(size--, objectSet.size());
            iterator.remove();
            Assert.assertEquals(size, objectSet.size());
        }
        Assert.assertEquals(0, objectSet.size());
        Assert.assertTrue(objectSet.isEmpty());

    }

    @Test
    public void testIteratorGeneric() {
        Set<Human> objectSet = new EqualSet();
        objectSet.add(new Human("John", 23, new GregorianCalendar(1987, 7, 7).getTime()));
        objectSet.add(new Human("Sam", 31, new GregorianCalendar(1988, 12, 23).getTime()));
        objectSet.add(new Human("William", 12, new GregorianCalendar(2003, 6, 1).getTime()));
        objectSet.add(new Human("Bob", 18, new GregorianCalendar(2001, 5, 16).getTime()));
        objectSet.add(null);

        Iterator<Human> iterator = objectSet.iterator();
        int size = objectSet.size();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            Assert.assertTrue(objectSet.contains(next));
            Assert.assertEquals(size--, objectSet.size());
            iterator.remove();
            Assert.assertEquals(size, objectSet.size());
        }
        Assert.assertEquals(0, objectSet.size());
        Assert.assertTrue(objectSet.isEmpty());
    }

    @Test
    public void testEquals() {
        Set<Integer> actual = new EqualSet<>(Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
        Set<Integer> expected = new EqualSet<>(Arrays.asList(new Integer[]{2, 3, 1, 5, 4}));
        Assert.assertTrue(actual.equals(expected));

        expected.add(6);
        Assert.assertFalse(actual.equals(expected));

        expected = null;
        Assert.assertFalse(actual.equals(expected));
    }

    @Test
    public void testHashCode() {
        Set<Integer> actual = new EqualSet<>(Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
        Set<Integer> expected = new EqualSet<>(Arrays.asList(new Integer[]{2, 3, 1, 5, 4}));
        Assert.assertTrue(actual.hashCode() == expected.hashCode());

        expected.add(6);
        Assert.assertFalse(actual.hashCode() == expected.hashCode());
    }
}
