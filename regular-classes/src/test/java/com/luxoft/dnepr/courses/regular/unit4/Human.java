package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Date;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: aram
 * Date: 09.11.13
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */
public class Human {
    private String name;
    private int age;
    private Date birthday;

    public Human() {
    }

    public Human(String name, int age, Date birthday) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (age != human.age) return false;
        if (birthday != null ? !birthday.equals(human.birthday) : human.birthday != null) return false;
        if (name != null ? !name.equals(human.name) : human.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return new Random().nextInt();
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

}
